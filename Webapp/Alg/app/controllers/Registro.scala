package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.db.slick._
import play.api.Play.current
import play.api.db.slick.Config.driver.simple.Session

import models._

case class DatosRegistro(user: DatosUsuario, razon_ingreso: String, passwordVerificado: String)


object Registro extends Controller with Secured {
  
  def formRegistro(implicit s: Session) = Form(
	mapping(
	    "usuario" -> mapping(
	    	"nombre" -> nonEmptyText,
	    	"password" -> nonEmptyText	    	
	    )(DatosUsuario.apply)(DatosUsuario.unapply),
	    "razon_ingreso" -> nonEmptyText,
	    "passVer" -> nonEmptyText
	)(DatosRegistro.apply)(DatosRegistro.unapply)
	verifying("El usuario ya existe!",
		x =>  x match {case DatosRegistro(DatosUsuario(u, _), _, _) => !Usuarios.findByNombre(u).isDefined } 
	)
	verifying("Las contraseñas no coinciden!", 
	    x => x match {case DatosRegistro(DatosUsuario(_, p), _, pv) => p == pv}
	)
  )

  def index = DBAction{ implicit rq =>
    Ok(views.html.registro(formRegistro))  
  }
  
  def verify = DBAction { implicit rq =>
    formRegistro.bindFromRequest.fold(
    	formError => BadRequest(views.html.registro(formError)), 
    	data => {
    	  val du = data.user
    	  Usuarios.insert(Usuario(0, du.nombre, "1", du.password))
    	  Redirect(routes.Login.index).flashing("success" -> "Usuario creado")
    	} 
    )
  }

}