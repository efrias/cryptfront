package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation._
import play.api.db.slick._
import play.api.Play.current
import play.api.db.slick.Config.driver.simple.Session

import models._
import util._

case class DatosUsuario(nombre: String, password: String)

object Login extends Controller with Secured{
  
  def formUsuario(implicit s: Session) = Form(
	mapping(
		"nombre" -> nonEmptyText,
		"password" -> nonEmptyText 
	)(DatosUsuario.apply)(DatosUsuario.unapply)
	verifying(Constraint[DatosUsuario]("constraints.passverify"){
		form => {
			val usuario = Usuarios.findByNombre(form.nombre)
			if(usuario.isDefined && Usuarios.authenticate(usuario.get.password, form.password))
				Valid
			else 
				Invalid("Usuario y/o contraseña incorrecta")
		}	  
	})
  )
  
  def index = DBAction { implicit rq =>
    session.get("username").map(_ => Ok(views.html.listaservicios())).getOrElse(Ok(views.html.login(formUsuario)))  
  }
  
  def logout = DBAction{ implicit rq =>
	Redirect(routes.Login.index).withNewSession
  }
  
  def authenticate = DBAction { implicit rq =>
    formUsuario.bindFromRequest.fold(
    	errorForm => BadRequest(views.html.login(errorForm)),
    	data =>{
    		println("logeado")
    		val usuario = Usuarios.findByNombre(data.nombre).get
    		Redirect(routes.Servicios.index).withSession("username" -> data.nombre, "admin" -> usuario.es_admin.toString).flashing("success" -> s"Bienvenido ${data.nombre}")
    	}
    )
  }

}