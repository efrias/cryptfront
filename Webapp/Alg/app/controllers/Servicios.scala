package controllers

import scala.sys.process._

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.db.slick._
import play.api.Play.current
import play.api.db.slick.Config.driver.simple.Session

import models._
import supuestos._
import util.ProcessJobs._


object Servicios extends Controller with Secured {
  import collection.immutable.ListMap
  
  val hidden = Form(
	single("code" -> text)
  )
  
  lazy val mapSupuestos = ListMap( 
	"factorizacion" -> Factorizacion,
	"dl" -> LogaritmoDiscreto,
	"dlec" -> LDCurvasElipticas,
	"hashc" -> ColisionesHash,
	"hashp" -> PreimagenesHash
  )
  
  def index = IsAuthenticatedR {user =>
    Action{ implicit rq =>
    	Ok(views.html.listaservicios()) 
    }
  }
  
  def supuestos = IsAuthenticatedR {user =>
  	Action{ implicit rq =>
  	  	Ok(views.html.listasupuestos(mapSupuestos)) 
  	}
  }
  
  def showParams(s: String) = IsAuthenticatedR {user =>  
  	Action{ implicit rq =>
  	  	val sup = mapSupuestos(s)
  		Ok(views.html.getAlgParams(sup, sup.form, s)) 
  	}
  }
  
  def verificarYEjecutar(s: String) = IsAuthenticatedR {user =>
    DBAction{ implicit ss =>
      	val sup = mapSupuestos(s)
    	sup.form.bindFromRequest.fold(
      		error => BadRequest(views.html.getAlgParams(sup, error, s)),
      		data => data.ejecutarAlg(user, sup)
      	)
    }
  }
   
  def subirTarea = IsAuthenticatedR { user => 
    Action{ implicit rq => 
    	Ok(views.html.getArchivoTarea(hidden))   
    }
  }
  
  def ejecutarTareaSinArchivo = IsAuthenticatedR {user =>
    DBAction{ implicit ss =>
    	hidden.bindFromRequest.fold(
      		error => BadRequest(views.html.getArchivoTarea(error)),
      		data => compilarYEjecutar(data, user)
      	)
    }
  }
  
  def ejecutarTarea = IsAuthenticatedR(parse.multipartFormData){user =>
    Action(parse.multipartFormData){ implicit r =>
		var lines = ""
		r.body.file("tarea").map{ file =>
		  	val rndm = scala.util.Random.nextInt
			val tmp = new java.io.File(s"/tmp/tarea$user-$rndm.scala")
			file.ref.moveTo(tmp)
			val scalatmp = io.Source.fromFile(s"/tmp/tarea$user-$rndm.scala")
			lines = scalatmp.getLines mkString "\n"
			scalatmp.close
			tmp.delete
			
			compilarYEjecutar(lines, user)
		}.getOrElse(BadRequest(views.html.listaservicios()))
		
    }
  }
  
  private def compilarYEjecutar(code: String, user: String)(implicit r: Request[_]) = {
	  "(play|controllers|views|models|util)(\\.[\\w\\d_]+)*(\\.\\{\\s*[\\w\\d_]+\\s*=>\\s*[\\w\\d_]+\\s*\\})?".r.
		findFirstIn(code).map{s =>
			BadRequest(views.html.getArchivoTarea(hidden.withGlobalError(s"No se puede usar el package $s").bind(Map("code" -> code))))
		}.getOrElse{
			val lines =	"import supuestos.base.Tarea\nnew Tarea{\n" +
					s"$code\n}"
		  
			import scala.reflect.runtime._
			import scala.tools.reflect.ToolBox
			import scala.tools.reflect.ToolBoxError
			import base.Tarea

			val cm = universe.runtimeMirror(getClass.getClassLoader)
			val tb = cm.mkToolBox()
			try{
				val tarea = tb.eval(tb.parse(lines)).asInstanceOf[Tarea]
				
				runThread(user, tarea.descripcion, tarea.run, Some(tarea), None)
		
				Redirect(routes.Servicios.index).flashing("success" -> "Tarea en proceso; revise su estado de tareas para ver su progreso")
			}catch{
			  	case e: ToolBoxError =>  BadRequest(views.html.getArchivoTarea(hidden.withGlobalError(e.message).bind(Map("code" -> code))))
			}
		}
  }

}