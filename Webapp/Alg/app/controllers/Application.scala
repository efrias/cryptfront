package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.db.slick._
import play.api.Play.current

import models._

object Application extends Controller {

  def index = DBAction { implicit rq =>
    //Query(Usuarios) foreach (x => println(x.nombreUsuario))
    
    Redirect(Usuarios.allString)
  }

}