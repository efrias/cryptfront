package controllers

import scala.sys.process._

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.db.slick._
import play.api.Play.current
import play.api.db.slick.Config.driver.simple.Session

import models._
import supuestos._
import util.ProcessJobs._


object TareasC extends Controller with Secured {
  
  def estadoTareas = IsAuthenticatedR { user =>
    DBAction{ implicit rq =>
    	Ok(views.html.estadoTareas(Estados.getEstadosByUsuario(user), false))
    }
  }
  
  def estadoTareasTodas = IsAdminR{ user =>
    DBAction{ implicit rq =>
      	Ok(views.html.estadoTareas(Estados.getTodos, true))
    }
  }
  
  def cancelar(id: Int) = canChange(id) { user =>
  	DBAction{ implicit rq =>
  		Estados.cancel(id, session.get("admin").map(x => if(x == "true") "un administrador" else "el usuario").get)
  		Redirect(routes.TareasC.estadoTareas).flashing("success" -> s"Se ha cancelado con éxito la tarea con id $id")
  	}
  }

}