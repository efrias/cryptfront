package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation._
import play.api.db.slick._
import play.api.Play.current
import play.api.db.slick.Config.driver.simple.Session

import models._

case class UsuarioRegistro(nombre: String)
case class CambioContraseña(pass: String, newPass: String, repeat: String)

object UsuariosC extends Controller with Secured{
  
  def formRegistro(implicit s: Session) = Form(
	mapping(
	    "usuario" -> nonEmptyText
	)(UsuarioRegistro.apply)(UsuarioRegistro.unapply)
	verifying("El usuario ya existe!",
		x =>  x match {case UsuarioRegistro(u) => !Usuarios.findByNombre(u).isDefined } 
	)
  )
  
  def formCambioContraseña(implicit s: DBSessionRequest[_]) = Form(
	mapping(
	    "pass" -> nonEmptyText,
	    "newpass" -> nonEmptyText,
	    "repeatpass" -> nonEmptyText
	)(CambioContraseña.apply)(CambioContraseña.unapply)
	verifying("La contraseña no es correcta!",
		x =>  x match {
		  case CambioContraseña(c, _, _) => {
		    val nombre_usuario = session.get("username").get
		    val usuario = Usuarios.findByNombre(nombre_usuario).get 
			Usuarios.authenticate(usuario.password, c) 
		  }
		} 
	)
	verifying("Las contraseñas no coinciden!",
		x => x match {case CambioContraseña(_, n, rn) => n == rn}
	)
  )
  
  def obtenerUsuario = IsAdminR{user =>
  	DBAction{ implicit rq =>
  	  	Ok(views.html.crearUsuario(formRegistro))
  	}
  }
  
  def crearUsuario = IsAdminR{ user => 
    DBAction { implicit rq =>
      	formRegistro.bindFromRequest.fold(
	    	formError => BadRequest(views.html.crearUsuario(formError)), 
	    	data => {
	    	  Usuarios.insert(Usuario(0, data.nombre, "1", "default"))
	    	  Redirect(routes.Servicios.index).flashing("success" -> s"Cuenta creada, el nombre de usuario es ${data.nombre} con contraseña default")
	    	} 
      	)
    }
  }


  def listaUsuarios = IsAdminR{ user =>
    DBAction { implicit rq =>
    	Ok(views.html.listaUsuarios(Usuarios.findNonAdminUsers))
    }
  }
  
  def promover(id: Int) = IsAdminR{ user =>
    DBAction{ implicit rq =>
      	Usuarios.findById(id).map{usuario =>
      	  	Usuarios.promover(id)
      	  	Redirect(routes.UsuariosC.listaUsuarios).flashing("success" -> s"Se ha promovido al usuario ${usuario.nombre_usuario} a administrador correctamente")
      	}.getOrElse(Redirect(routes.UsuariosC.listaUsuarios).flashing("error" -> s"El usuario al que se ha intentado promover, no existe"))
    }
  }

  def cambiarCsña = IsAuthenticatedR{ user =>
    DBAction{ implicit rq =>
      	Ok(views.html.cambiarPassword(formCambioContraseña))
    }
  }
  
  def verificarCambio = IsAuthenticatedR{ user => 
  	DBAction{ implicit rq =>
  		formCambioContraseña.bindFromRequest.fold(
  			error => BadRequest(views.html.cambiarPassword(error)),
  			data => {
  				Usuarios.nuevaContraseña(user, data.newPass)
  				Redirect(routes.Servicios.index).flashing("success" -> "Se ha cambiado la contraseña correctamente")
  			}
  		)
  	}
  }
  
}