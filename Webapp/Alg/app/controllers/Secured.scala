package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.db.slick._
import play.api.Play.current
import play.api.mvc.Results._
import play.api.libs.iteratee._
import scala.concurrent.Future

import models._

trait Secured { 
  
  /**
   * Retrieve the connected user email.
   */
  private def username(request: RequestHeader) = request.session.get("username")

  /**
   * Redirect to login if the user in not authorized.
   */
  private def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.Login.index).flashing("error" -> "Primero debe logearse antes de entrar al sistema")
  
  // --
  
  private def adminUsername(request: RequestHeader) = {
	val a = request.session.get("admin").getOrElse("false")
	if(a == "false") None else request.session.get("username")
  }
  
  private def onUnauthorizedAdmin(request: RequestHeader) = Results.Redirect(routes.Login.index).flashing("error" -> "Solo un administrador tiene acceso")
  
  /** 
   * Action for authenticated users.
   */
  
  def Authenticated[A](
    userinfo: RequestHeader => Option[A],
    onUnauthorized: RequestHeader => SimpleResult)(action: A => EssentialAction): EssentialAction = {

    EssentialAction { request =>
      userinfo(request).map { user =>
        action(user)(request)
      }.getOrElse {
        Done(onUnauthorized(request), Input.Empty)
      }
    }

  }
  
  def IsAuthenticated(f: => String => Request[AnyContent] => Action[AnyContent]) = Security.Authenticated(username, onUnauthorized) { user =>
    Action.async(request => f(user)(request)(request))
  }
  
  def IsAuthenticatedR(f: => String => Action[AnyContent]) = Security.Authenticated(username, onUnauthorized){ user =>
    Action.async(request => f(user)(request))
  }
  
  def IsAuthenticatedR[T](bodyparser: BodyParser[T])(f: => String => Action[T]) = Security.Authenticated(username, onUnauthorized){ user =>
    Action.async[T](bodyparser)(request => f(user)(request))
  }
  
  def IsAdminR(f: => String => Action[AnyContent]) = Security.Authenticated(adminUsername, onUnauthorizedAdmin){ user =>
    Action.async(request => f(user)(request))
  }
  
  def canChange(job: Int)(f: => String => Action[AnyContent]) = IsAuthenticated{ user => request =>
	DB.withSession{ implicit s =>
	  	if(Estados.isOwner(user, job) || adminUsername(request).isDefined){
	  		if(!Estados.canBeCancelled(job)){
	  			Action{Results.Redirect(routes.Login.index).flashing("error" -> "La tarea ya esta terminada!")}
	  		}
	  		else f(user)
		}
		else{
			Action{Results.Redirect(routes.Login.index).flashing("error" -> s"Usted no es dueño de la tarea número $job")}
		}
	}
  }

  /**
   * Check if the connected user is a member of this project.
   */
  /*def IsMemberOf(project: Long)(f: => String => Request[AnyContent] => Result) = IsAuthenticated { user => request =>
    if(Project.isMember(project, user)) {
      f(user)(request)
    } else {
      Results.Forbidden
    }
  }*/
}