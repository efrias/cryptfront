package supuestos

import scala.sys.process._

import supuestos.base._

case class EncontrarColision(hash: String, tr: Int, bytesZero: Int, cores: Int) extends Supuesto{
	def params = hash :: tr.toString :: bytesZero.toString :: cores.toString :: Nil
	def process = Process(sshRemote(s"./hash_collisions $hash $tr $bytesZero"))
}

object ColisionConstExtr extends ConstExtrSupuesto{
	override def apply[A, B](x: A, y: B) = (x, y) match{  
	  case (a: String, (b: Int, (c: Int, d: Int))) => EncontrarColision(a, b, c, d)
	}
}

case class SelectWithText(s: String, hashes: List[String]) extends Select(s, hashes) with Text with NonEmpty   

object ColisionesHash extends InputSupuesto{
	def hashes = List("SHA1", "SHA224", "SHA384", "SHA512", "SHA3-224", "SHA3-256", "SHA3-384", "SHA3-512", "MD2", "MD4", "MD5", "Tiger", "Whirlpool", "RIPEMD160", "RIPEMD320", "RIPEMD128")  
	val nombre = "Colisiones de hash"
	val f = ColisionConstExtr
	val fields = List(SelectWithText("Hash a usar", hashes), InputTextWithNumber("Bytes truncacion (0 si no se trunca)"), InputTextWithNumber("Bytes en cero"), InputTextWithCores("Cores"))
}
