package supuestos

import scala.sys.process._

import supuestos.base._

case class DLEC(px: BigInt, py: BigInt, a: BigInt, b: BigInt, p: BigInt, gx: BigInt, gy: BigInt, cores: Int) extends Supuesto{
	def params = px.toString :: py.toString :: a.toString :: b.toString :: p.toString :: gx.toString :: gy.toString :: cores.toString :: Nil
	def process = Process(sshRemote(s"./brute_force_dl_ec $px $py $a $b $p $gx $gy"))
}

object DLECConstExtr extends ConstExtrSupuesto{
	override def apply[A, B](x: A, y: B) = (x, y) match{ 
	  case (a: BigInt, (b: BigInt, (c: BigInt, (d: BigInt, (e: BigInt, (f: BigInt, (g: BigInt, h: Int))))))) => DLEC(a, b, c, d, e, f, g, h)
	}
	
} 

object LDCurvasElipticas extends InputSupuesto{
	val nombre = "Logaritmo Discreto sobre Curvas Elipticas"
	val f = DLECConstExtr
	val fields = List(InputTextWithBigInt("Introduzca componente x del punto"), InputTextWithBigInt("Introduzca componente y del punto"), InputTextWithBigInt("Introduzca componente a de la curva"), InputTextWithBigInt("Introduzca componente b de la curva"), InputTextWithBigInt("Primo p (grupo)"), InputTextWithBigInt("Introduzca componente x del generador"), InputTextWithBigInt("Introduzca componente y del generador"), InputTextWithCores("Cores"))
}
