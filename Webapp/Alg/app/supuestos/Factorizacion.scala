package supuestos

import scala.sys.process._

import supuestos.base._
import util.ProcessJobs._
import util.Scheduler

case class Factorizar(n: BigInt, cores: Int) extends Supuesto{
	def params = n.toString :: cores.toString :: Nil
	def process = Process(sshRemote(s"./trial_division ${n}"))
	override def run(implicit cancellable: Option[MutableCancellable] = None) = {
		execute {
			val s = process.!!
			val pattern = "\\[([0-9]+) ([0-9]+)\\]".r    
	      
			val seq = (for(m <- pattern.findAllMatchIn(s)) yield m.group(1)+(if(m.group(2) != "1") "^"+m.group(2) else "")).toList
			seq.reduce(_+"·"+_)
		}
	}
}

object FactConstExtr extends ConstExtrSupuesto{
	override def apply[A, B](x: A, y: B) = (x, y) match{ 
	  case (a: BigInt, b: Int) => Factorizar(a, b)
	}
	
}

case class InputTextWithBigInt(s: String) extends InputText(s) with BigInteger 
case class InputTextWithNumber(s: String) extends InputText(s) with Number
case class InputTextWithCores(s: String) extends InputText(s"$s (Max. ${Scheduler.coresTotales})") with Cores

object Factorizacion extends InputSupuesto{
	val nombre = "Factorización"
	val f = FactConstExtr
	val fields = List(InputTextWithBigInt("Número a factorizar"), InputTextWithCores("Cores"))
}
