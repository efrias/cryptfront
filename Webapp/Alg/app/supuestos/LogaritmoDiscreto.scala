package supuestos

import scala.sys.process._

import supuestos.base._

case class DL(y: BigInt, p: BigInt, g: BigInt, cores: Int) extends Supuesto{
	def params = y.toString :: p.toString :: g.toString :: cores.toString :: Nil
	def process = Process(sshRemote(s"./brute_force_dl $y $p $g"))
}

object DLConstExtr extends ConstExtrSupuesto{
	override def apply[A, B](x: A, y: B) = (x, y) match{ 
	  case (a: BigInt, (b: BigInt, (c: BigInt, d: Int))) => DL(a, b, c, d)
	}
	
}

object LogaritmoDiscreto extends InputSupuesto{
	val nombre = "Logaritmo Discreto"
	val f = DLConstExtr
	val fields = List(InputTextWithBigInt("Introduzca número a calcular el logaritmo"), InputTextWithBigInt("Grupo p (primo)"), InputTextWithBigInt("Generador"), InputTextWithCores("Cores"))
}
