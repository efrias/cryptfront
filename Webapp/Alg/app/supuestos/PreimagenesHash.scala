package supuestos

import scala.sys.process._

import supuestos.base._

case class EncontrarPreimagen(hash: String, imagen: String, tr: Int, cores: Int) extends Supuesto{
	def params = hash :: imagen :: tr.toString :: cores.toString :: Nil
	def process = Process(sshRemote(s"./hash_preimage $hash $imagen $tr"))
}

object PreimagenConstExtr extends ConstExtrSupuesto{
	override def apply[A, B](x: A, y: B) = (x, y) match{ 
	  case (a: String, (b: String, (c: Int, d: Int))) => EncontrarPreimagen(a, b, c, d)
	}
}

case class InputTextWithTextHex(s: String) extends InputText(s) with Text with Hex

object PreimagenesHash extends InputSupuesto{
	def hashes = List("SHA1", "SHA224", "SHA384", "SHA512", "SHA3-224", "SHA3-256", "SHA3-384", "SHA3-512", "MD2", "MD4", "MD5", "Tiger", "Whirlpool", "RIPEMD160", "RIPEMD320", "RIPEMD128")  
	val nombre = "Preimagenes hash"
	val f = PreimagenConstExtr
	val fields = List(SelectWithText("Hash a usar", hashes), InputTextWithTextHex("Secuencia a encontrar una preimagen"), InputTextWithNumber("Bytes truncacion (0 si no se trunca)"), InputTextWithCores("Cores"))
}
