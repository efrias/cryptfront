package supuestos.base

import play.api.data._
import play.api.data.Forms._

import play.api.data.format.Formats._
import play.api.data.format._

import util.Scheduler

trait Validation {
	type A
	def constraint: Mapping[A] 
}

trait Text extends Validation{
	type A = String
	def constraint = text
}

trait BigDec extends Validation{
	type A = BigDecimal
	def constraint = bigDecimal
}

trait BigInteger extends Validation{
	implicit val bigIntFormat: Formatter[BigInt] = new Formatter[BigInt] {

	    override val format = Some(("format.number", Nil))
	
	    def bind(key: String, data: Map[String, String]) = {
	      Formats.stringFormat.bind(key, data).right.flatMap { s =>
	        scala.util.control.Exception.allCatch[BigInt]
	          .either {
	            val bd = BigInt(s)
	            bd
	          }
	          .left.map { e =>
	            Seq(
	              FormError(key, "error.number", Nil)
	            )
	          }
	      }
	    }
	
	    def unbind(key: String, value: BigInt) = Map(key -> value.toString)
	}
	 
	type A = BigInt
	def constraint = of[BigInt]
}

trait Number extends Validation{
	type A = Int
	def constraint = number
}

trait Cores extends Number{
	override def constraint = number verifying(s"La cantidad de cores debe ser menor o igual a ${Scheduler.coresTotales}",
	    x => x <= Scheduler.coresTotales)
}

trait NonEmpty extends Validation{
	abstract override def constraint = super.constraint.verifying("El campo debe rellenarse", 
	    x => x != "")
}

trait Hex extends Validation{
	abstract override def constraint = super.constraint.verifying("El campo debe ser una secuencia en hexadecimal",
	    x => x match {
	      case s: String => s.filterNot(c => (c >= '0' && c <= '9') || (c.toLower >= 'a' && c.toLower <= 'f')).isEmpty
	      
	    }
	  )
}