package supuestos.base

import scala.sys.process._

import play.api.data.Forms._
import play.api.data._
import play.api.mvc._
import play.api.Play.current
import play.api.db.slick.Config.driver.simple.Session
import play.api.db.slick._
import play.api._
import scala.concurrent.duration._
import akka.actor._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.immutable.List

import models._
import controllers._
import util.ProcessJobs._
import util.Scheduler
 
trait Supuesto{
	var resources: List[String] = List.empty
	var idEstado: Int = _
	
	def cores: Int
	def params: List[String]
	def sshRemote(alg: String): Seq[String] = {
		Seq("ssh", "-i", "/home/ec2-user/Key1.pem", "root@172.31.17.231", s"cd /data/memoria/Algoritmos ; ./pidexecute.sh $idEstado mpirun -hosts ${Scheduler.hostList(resources)} -np ${cores} $alg")
	}
	private[this] def entrada(alg: InputSupuesto): String = alg.fields.zip(params).foldLeft("Algoritmo: "+alg.nombre)((s, f) => f match{case (n, p) => s+","+n.nombre+": "+p})
	def process: ProcessBuilder
	protected def execute(r: => String)(implicit cancellable: Option[MutableCancellable] = None) = {
		cancellable.map{c => c.cancel; idEstado = c.id}
		
		Scheduler.takeResources(this)
		  
		println(resources) 
		println(Scheduler.resources)
		
		val result = r
		
		Scheduler.freeResources(this)
		
		cancellable.map{c => c.start}
		
		result
	}
	def run(implicit cancellable: Option[MutableCancellable] = None): String = {
		
		execute({process.!!})
	}
	def ejecutarAlg(username: String, alg: InputSupuesto)(implicit flash: Flash): SimpleResult = {
		runThread(username, entrada(alg), run, None, Some(this))
	  
		Results.Redirect(routes.Servicios.index).flashing("success" -> "Trabajo en proceso; dirijase al estado de tareas para ver su progreso")
	}
}

trait ConstExtrSupuesto{
	def apply[A, B](x: A, y: B): Supuesto = throw new Exception("apply deberia estar implementado!")
	def unapply[A, B](x: Supuesto): Option[(A,B)] = None
	
	def apply1[A](x: A): Supuesto = throw new Exception("apply1 deberia estar implementado!")
	def unapply1[A](x: Supuesto): Option[A] = None
}

trait InputSupuesto {
	private def genMapping(listFV: List[FieldWithValidation]): Mapping[_] = listFV match{
	  case x :: Nil => single(x.nombre, x.constraint)
	  case x :: xs => { 
	  	tuple(
		  x.nombre -> x.constraint,
		  "" -> genMapping(xs)
		)
	  }
	}
  
	def form: Form[Supuesto] = 
	  Form(
	      if(fields.tail.isEmpty){
	        mapping(
	        	fields.head.nombre -> fields.head.constraint
		    )(f.apply1)(f.unapply1)
	      }
	        
		  else{
		    mapping(
				fields.head.nombre -> fields.head.constraint,
				"" -> genMapping(fields.tail)
			)(f.apply)(f.unapply) 
		  } 
		    
	  ) 
	
	def f: ConstExtrSupuesto
	def nombre: String
	def fields: List[FieldWithValidation]
}
