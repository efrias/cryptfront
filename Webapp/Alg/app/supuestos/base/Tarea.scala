package supuestos.base

import util.ProcessJobs._

trait Tarea {
	implicit var cancellable: Option[MutableCancellable] = None
  
	def descripcion: String = "Tarea Personalizada"
	def run: String
}