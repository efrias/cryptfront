package supuestos

package object base{
	type FieldWithValidation = HtmlField with Validation
	implicit def stringToHtml(s: String): play.api.templates.Html = new play.api.templates.Html(new StringBuilder(s))
}