package supuestos.base

import play.api.templates.Html
import play.api.templates.PlayMagic._



trait HtmlField {
	def nombre: String
	val inputDef: (String, String, Option[String], Map[Symbol, Any]) => Html 
}

class InputText(val nombre: String) extends HtmlField{
	val inputDef = (id: String, name: String, value: Option[String], args: Map[Symbol, Any]) => {
		"<input type=\"text\" name=\""+name+"\" id=\""+id+"\" value=\""+value.getOrElse("")+"\" "+toHtmlArgs(args)+">" : Html
	} 
}

class Select(val nombre: String, val options: List[String]) extends HtmlField{
	val inputDef = (id: String, name: String, value: Option[String], args: Map[Symbol, Any]) => {
		val htmlargs = toHtmlArgs(args)
		val optionsstring = options.map{o =>
		  val selected = value.map(v => if(o == v) "selected" else "").getOrElse("")
		  s"""<option value="$o" $selected>$o</option>\n"""
		}.reduce(_+_)
		s"""<select id="$id" name="$name" $htmlargs> $optionsstring </select>""": Html 
	}
}