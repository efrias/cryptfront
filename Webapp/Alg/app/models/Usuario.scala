package models

import play.api.db.slick.Config.driver.simple._

import org.mindrot.jbcrypt.BCrypt

case class Usuario(id: Int, nombre_usuario: String, semilla: String, password: String, es_admin: Boolean = false)

object Usuarios extends Table[Usuario]("usuarios") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc, O.NotNull)
  def nombreUsuario = column[String]("nombre_usuario", O.NotNull)
  def semilla = column[String]("semilla", O.NotNull)
  def password = column[String]("password", O.NotNull)
  def es_admin = column[Boolean]("es_admin", O.NotNull)
 
  // Every table needs a * projection with the same type as the table's type parameter
  def * = id ~ nombreUsuario ~ semilla ~ password ~ es_admin <> (Usuario.apply _, Usuario.unapply _)
  
  def autoInc = * returning id
  
  val byNombre = createFinderBy(_.nombreUsuario)
  val byId = createFinderBy(_.id)
  
  def allString(implicit s: Session): String = {
    val users = for {u <- Usuarios} yield (u.id, u.nombreUsuario)
    
    val ql = Query(Usuarios)
    
    users.list.map({case (id, nu) => id+" "+nu+"\n"}).mkString
  }
  
  
  
  def insert(user: Usuario)(implicit s: Session) = {
    val salt = BCrypt.gensalt(12)
    val pw = BCrypt.hashpw(user.password, salt) 
    this.autoInc.insert(Usuario(user.id, user.nombre_usuario, salt, pw))
  }
  
  def findByNombre(nombre: String)(implicit s: Session) = this.byNombre(nombre).firstOption
  def findById(id: Int)(implicit s: Session) = this.byId(id).firstOption
  
  def authenticate(hashpass: String, password: String)(implicit s: Session) = BCrypt.checkpw(password, hashpass)

  def findNonAdminUsers(implicit s: Session) = {
    val users = for(u <- Usuarios if u.es_admin === false) yield u
    
    users.list
  }
  
  def promover(id: Int)(implicit s: Session) = {
    val q = for(u <- Usuarios if u.id === id) yield u.es_admin
    q.update(true)
  }
  
  def nuevaContraseña(user: String, pass: String)(implicit s: Session) = {
    val q = for(u <- Usuarios if u.nombreUsuario === user) yield u.password
    
    val salt = BCrypt.gensalt(12)
    val pw = BCrypt.hashpw(pass, salt)
    
    q.update(pw)
  }
}