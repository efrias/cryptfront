package models

import play.api.db.slick.Config.driver.simple._
import scala.sys.process._

import util.Scheduler
import util.Utils._

import org.joda.time.{DateTime, Period, Duration}
import com.github.tototoshi.slick.JodaSupport._

case class Estado(id: Int, listo: Int, entrada: String, tiempo_inicial: DateTime, tiempo_transcurrido: String, resultado: String, usuario_id: Int)

object Estados extends Table[Estado]("estados") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc, O.NotNull)
  def listo = column[Int]("listo", O.NotNull, O.DBType("SMALLINT(2)"))
  def entrada = column[String]("entrada", O.NotNull)
  def tiempo_inicial = column[DateTime]("tiempo_inicial", O.NotNull)
  def tiempo_transcurrido = column[String]("tiempo_transcurrido", O.NotNull)
  def resultado = column[String]("resultado", O.NotNull)
  def pid = column[Int]("pid", O.NotNull, O.DBType("INT(16)"))
  def usuario_id = column[Int]("usuario_id", O.NotNull)
  
  def usuarios = foreignKey("usuario_id", usuario_id, Usuarios)(_.id)
 
  // Every table needs a * projection with the same type as the table's type parameter
  def * = id ~ listo ~ entrada ~ tiempo_inicial ~ tiempo_transcurrido ~ resultado ~ usuario_id <> (Estado.apply _, Estado.unapply _)
  
  def autoInc = * returning id
  
  val byId = createFinderBy(_.id)
  
  def insert(username: String, entrada: String)(implicit s: Session) = {
    val u = Usuarios.findByNombre(username).get
    val estado = Estado(0, 0, entrada, DateTime.now, "0", "", u.id)
    this.autoInc.insert(estado)
  }
  
  def findById(id: Int)(implicit s: Session) = this.byId(id).firstOption
  
  def update(id: Int, resultado: String)(implicit s: Session) = {
    val q = for(e <- Estados if e.id === id) yield e.listo ~ e.tiempo_inicial ~ e.tiempo_transcurrido ~ e.resultado
    
    val tiempo = calculateTime(q.list.head._2, q.list.head._3)

      		
    q.update((1, q.list.head._2, tiempo, resultado))
  }
  
  def updateForCancelled(id: Int)(implicit s: Session) = {
	val q = for(e <- Estados if e.id === id) yield e.listo ~ e.tiempo_inicial ~ e.tiempo_transcurrido ~ e.resultado
	val tiempo = calculateTime(q.list.head._2, q.list.head._3)
	
	q.update((2, q.list.head._2, tiempo, "Tiempo permitido excedido"))
  }
  
  def updateProcess(id: Int)(implicit s: Session) = {
    val q = for(e <- Estados if e.id === id) yield e.listo ~ e.tiempo_inicial
	q.update((0, DateTime.now))
  }
  
  def updateWaiting(id: Int)(implicit s: Session) = {
    val q = for(e <- Estados if e.id === id) yield e.listo ~ e.tiempo_inicial ~ e.tiempo_transcurrido
	q.update((3, q.list.head._2, calculateTime(q.list.head._2, q.list.head._3)))
  }
  
  def getEstadosByUsuario(usuario: String)(implicit s: Session) = {
    val q = for{
        u <- Usuarios
        e <- Estados if u.id === e.usuario_id && u.nombreUsuario === usuario
    } yield (e, u.nombreUsuario)
    q.list
  }
  
  def isOwner(usuario: String, id: Int)(implicit s: Session) = {
    findById(id).map(e => Usuarios.findByNombre(usuario).get.id == e.usuario_id).getOrElse(false)
  }
  
  def canBeCancelled(id: Int)(implicit s: Session) = {
    findById(id).map(e => List(0, 3).contains(e.listo)).getOrElse(false)
  }
  
  def cancel(id: Int, quien: String)(implicit s: Session) = {
	val q = for(e <- Estados if e.id === id) yield e.listo ~ e.tiempo_inicial ~ e.tiempo_transcurrido ~ e.resultado ~ e.pid
    
    val tiempo = if(q.list.head._1 == 0) calculateTime(q.list.head._2, q.list.head._3) else q.list.head._3
	
	Scheduler.processQueue.synchronized{
		Scheduler.tareas(id)._2.stop
		Scheduler.tareas(id)._1.map(s => {Scheduler.freeResources(s); Scheduler.processQueue.dequeueFirst(x => x eq s);})
		Scheduler.tareas -= id
	}
	
	Process(Seq("ssh", "-i", "/home/ec2-user/Key1.pem", "root@172.31.17.231", s"kill -9 ${q.list.head._5}")).!!
	q.update((2, q.list.head._2, tiempo, s"Cancelado por $quien", 0))
  }
  
  def getTodos(implicit s: Session) = {
	val q = for{
        u <- Usuarios
        e <- Estados if u.id === e.usuario_id
    } yield (e, u.nombreUsuario)
    
    q.list
  }
}