package util

import models._
import play.api.db.slick._
import play.api.Play.current
import akka.actor._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

import supuestos.base.Tarea
import supuestos.base.Supuesto

object ProcessJobs {
	class MutableCancellable(val id: Int){
		var c: Cancellable = _
		var deadline: Deadline = _
		var timeleft: FiniteDuration = 60 seconds
		var cancelled: Int = 1
		
		def cancel = {
		  	cancelled += 1
		  	if(cancelled == 1){
			  	c.cancel
			  	timeleft = deadline.timeLeft  		
		  	}
		  	
		}
		
		def start = {
		  	cancelled -= 1
		  	if(cancelled == 0){
		  		val th = Thread.currentThread
			  	deadline = timeleft.fromNow
			  	c = Global.system.scheduler.scheduleOnce(timeleft)(callbackScheduler(th, id))
		  	}		  	
		}
	}
	
	def callbackScheduler(t: Thread, id: Int) = {
		Scheduler.processQueue.synchronized{
			t.stop
			Scheduler.tareas.get(id).map(k => k._1.map(s => Scheduler.freeResources(s)))
			Scheduler.tareas -= id
		}
		println("cancelado")
		DB.withSession{implicit s: Session =>
			Estados.updateForCancelled(id)
		}
	}
	
	def processWaiting(id: Int) = {
		DB.withSession{implicit s: Session =>
			Estados.updateWaiting(id)
		}
	}
	
	def processing(id: Int) = {
		DB.withSession{implicit s: Session =>
			Estados.updateProcess(id)
		}
	}
  
	def runThread(user: String, entrada: String, resultado: => String, tarea: Option[Tarea] = None, sup: Option[Supuesto]) = {
		val system = Global.system
  	  	
  		new Thread(
  		    new Runnable{
				def run = {
					DB.withSession{ implicit s: Session =>
					  	try{
					  		val th = Thread.currentThread
						  	val id = Estados.insert(user, entrada)
						  	Scheduler.tareas += ((id, (None, th)))
		  		  		  	
		  		  		  	val cancellable: MutableCancellable = new MutableCancellable(id)
					  		tarea.map{t => t.cancellable = Some(cancellable)}
					  		sup.map{s => s.idEstado = id}
					  		
		  		  		  	tarea.map{t => cancellable.start}
						  	val res = resultado
						  	tarea.map{t => cancellable.cancel}
							Estados.update(id, res)
							Scheduler.tareas -= id
					  	}catch{
					  	  	case e: Error => throw e 
					  	}
	  		  		  	
					}
				}
			}
		).start()
  		
  		
	}
}
