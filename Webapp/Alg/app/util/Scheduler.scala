package util

import play.api._
import models._
import play.api.db.slick._
import scala.collection.mutable.{SynchronizedQueue, Queue, HashMap}
import supuestos.base.Supuesto
import util.ProcessJobs._

object Scheduler {
  
  val tareas: HashMap[Int, (Option[Supuesto], Thread)] = new HashMap
  
  val resources: SynchronizedQueue[String] = new SynchronizedQueue
  val processQueue: SynchronizedQueue[Supuesto] = new SynchronizedQueue
  
  val coresTotales: Int = {
    val pattern = "(.*):([1-9]+[0-9]*)".r 
    var cores = 0
	val source = scala.io.Source.fromFile("/data/hosts")
	for(x <- source.getLines){
		for(m <- pattern.findAllMatchIn(x)){
			val c = m.group(2).toInt
			cores = cores + c
			
			resources ++= List.fill(c)(m.group(1))
		}
	}   
    
    println(resources)
    cores 
  }
  
  def hostList(res: List[String]): String = {
      val dist = res.distinct
	  dist.map(x => x+":"+res.count(y => y == x)).reduce(_+","+_)
  }
  
  var coresDisponibles: Int = _
  
  def allocateResources(cantidad: Int): List[String] = {
      coresDisponibles -= cantidad
      var cores = cantidad
      resources.dequeueAll(x => {cores = cores - 1; cores >= 0}).toList
  }
  
  def takeResources(sup: Supuesto) = {
      processQueue.synchronized{
    	  tareas += ((sup.idEstado, (Some(sup), Thread.currentThread)))
		  processQueue += sup
		  processWaiting(sup.idEstado)
		  while(sup.cores > coresDisponibles || (processQueue.head ne sup)){
			  println("no hay recursos y encolo proceso")
			  processQueue.wait
		  }
		  processing(sup.idEstado)
		  val ver = processQueue.dequeue
		  if(ver ne sup)
			  println("aca hay un error grave!")
			  
		  println("pido recursos")
		  
		  sup.resources = allocateResources(sup.cores)
	  }
  }
  
  def freeResources(sup: Supuesto) = {
	  processQueue.synchronized{
		  tareas += ((sup.idEstado, (None, Thread.currentThread)))
		  resources ++= sup.resources
		  coresDisponibles += sup.resources.length
	      processQueue.headOption.map(s => processQueue.notifyAll)
	  } 
	  println("libere "+sup.resources)
	  println("global "+resources)
  }
  
  def init = {
      coresDisponibles = coresTotales
  }

}