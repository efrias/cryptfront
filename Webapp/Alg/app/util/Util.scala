package util

import org.joda.time.{Period, Duration, DateTime}
import scala.collection.mutable.HashMap
import models._

object Utils{
	def periodToString(period: Period): String = {
		var s = ""
		if(period.getYears != 0) s += period.getYears+" Año(s),"
		if(period.getMonths != 0) s += period.getMonths+" Mese(s),"
		if(period.getWeeks != 0) s += period.getWeeks+" Semana(s),"
		if(period.getDays != 0) s += period.getDays+" Dia(s),"
		if(period.getHours != 0) s += period.getHours+" Hora(s),"
		if(period.getMinutes != 0) s += period.getMinutes+" Minuto(s),"
		s += period.getSeconds+" Segundo(s)"
		s
	}
	
	def stringToPeriod(s: String): Period = {
		val nodefaultmap: HashMap[String, Int] = new HashMap
		s.split(',').map(t => nodefaultmap += ((t.split(' ')(1), t.split(' ')(0).toInt)))
		val map = nodefaultmap.withDefault((x: String) => 0)
		new Period(map("Año(s)"), map("Mese(s)"), map("Semana(s)"), map("Dia(s)"), map("Hora(s)"), map("Minuto(s)"), map("Segundo(s)"), 0)
	}
	
	def timeToShow(e: Estado): String = {
		if(List(1, 2, 3).contains(e.listo))
			e.tiempo_transcurrido
		else{
			calculateTime(e.tiempo_inicial, e.tiempo_transcurrido)
		}
	  
	}
	
	def calculateTime(tiempo_inicial: DateTime, tiempo_transcurrido: String): String = {
		val tiempoUltimaEjecucion = (new Duration(tiempo_inicial, DateTime.now)).toPeriod(null, null)
		if(tiempo_transcurrido == "0") 
      		periodToString(tiempoUltimaEjecucion)
      	else{
      		periodToString(stringToPeriod(tiempo_transcurrido).plus(tiempoUltimaEjecucion))
      	}
	}
}