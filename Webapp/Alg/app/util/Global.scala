package util

import play.api._
import models._
import play.api.db.slick._
import akka.actor.ActorSystem
import supuestos.base.Supuesto

object Global extends GlobalSettings {

  val system = ActorSystem("SupuestosSystem")
  
  override def onStart(app: Application) {
	  Scheduler.init
  } 

}