-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 24-01-2014 a las 18:51:12
-- Versión del servidor: 5.6.14
-- Versión de PHP: 5.5.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cryptfront_bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE IF NOT EXISTS `estados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `listo` smallint(2) NOT NULL,
  `entrada` text NOT NULL,
  `resultado` text NOT NULL,
  `usuario_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_id` (`usuario_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=110 ;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `listo`, `entrada`, `resultado`, `usuario_id`) VALUES
(1, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:8801345111111111,Cores:2', '[[13 1] [457 1] [557 1] [2287 1] [1162969 1]]\n', 22),
(2, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:8801345111111111,Cores:2', '[[13 1] [457 1] [557 1] [2287 1] [1162969 1]]\n', 22),
(3, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:8801345111111111,Cores:2', '[[13 1] [457 1] [557 1] [2287 1] [1162969 1]]\n', 22),
(4, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:324324,Cores:2', '[[2 2] [3 4] [7 1] [11 1] [13 1]]\n', 22),
(5, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:789776578899871,Cores:2', '[[8221 1] [96068188651 1]]\n', 22),
(6, 1, 'Algoritmo: Logaritmo Discreto,Introduzca número a calcular el logaritmo:2,Grupo p (primo):7,Generador:3,Cores:2', '2\n', 22),
(7, 1, 'Algoritmo: Logaritmo Discreto sobre Curvas Elipticas,Introduzca componente x del punto:1,Introduzca componente y del punto:5,Introduzca componente a de la curva:3,Introduzca componente b de la curva:8,Primo p (grupo):13,Introduzca componente x del generador:1,Introduzca componente y del generador:8,Cores:2', '8\n', 22),
(8, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:1221432134,Cores:2', '[[2 1] [13 1] [233 1] [201623 1]]\n', 22),
(11, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:234234234,Cores:2', '[[2 1] [3 3] [13 1] [333667 1]]\n', 22),
(12, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:21321421312,Cores:2', '[[2 9] [41643401 1]]\n', 22),
(15, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:21343254234,Cores:2', '[[2 1] [3 1] [577 1] [6165007 1]]\n', 22),
(16, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:3243242342,Cores:2', '[[2 1] [349 1] [4646479 1]]\n', 22),
(17, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:3243242342,Cores:2', '[[2 1] [349 1] [4646479 1]]\n', 22),
(18, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:3243242342,Cores:2', '[[2 1] [349 1] [4646479 1]]\n', 22),
(19, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:3243242342,Cores:2', '[[2 1] [349 1] [4646479 1]]\n', 22),
(20, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:3243242342,Cores:2', '[[2 1] [349 1] [4646479 1]]\n', 22),
(21, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:3243242342,Cores:2', '[[2 1] [349 1] [4646479 1]]\n', 22),
(22, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:3243242342,Cores:2', '[[2 1] [349 1] [4646479 1]]\n', 22),
(23, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:3243242342,Cores:2', '[[2 1] [349 1] [4646479 1]]\n', 22),
(24, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:3243242342,Cores:2', '[[2 1] [349 1] [4646479 1]]\n', 22),
(25, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:3243242342,Cores:2', '[[2 1] [349 1] [4646479 1]]\n', 22),
(26, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:2132423423,Cores:2', '[[13 1] [2843 1] [57697 1]]\n', 22),
(27, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:3243243242,Cores:2', '[[2 1] [61 1] [131 1] [202931 1]]\n', 22),
(28, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:676776565676,Cores:2', '[[2 2] [101 1] [1675189519 1]]\n', 22),
(29, 1, 'RSA con n=3337 y e=79', '344558624', 22),
(30, 1, 'RSA con n=3337 y e=79', '344558624', 22),
(31, 1, 'RSA con n=3337 y e=79', '344558624', 22),
(32, 1, 'RSA con n=3337 y e=79', '1019', 22),
(33, 1, 'RSA con n=3337 y e=79', '1019', 22),
(34, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:3242425,Cores:2', '[[5 2] [23 1] [5639 1]]\n', 22),
(35, 1, 'RSA con n=3337 y e=79', '1019', 22),
(36, 1, 'RSA con n=3337 y e=79', '1019', 22),
(37, 1, 'RSA con n=3337 y e=79', '1019', 22),
(38, 1, 'RSA con n=3337 y e=79', '1019', 22),
(39, 1, 'RSA con n=3337 y e=79', '1019', 22),
(40, 1, 'RSA con n=3337 y e=79', '1019', 22),
(41, 1, 'RSA con n=3337 y e=79', '1019', 22),
(42, 1, 'RSA con n=3337 y e=79', '1019', 22),
(43, 1, 'RSA con n=3337 y e=79', '1019', 22),
(44, 1, 'RSA con n=3337 y e=79', '1019', 22),
(45, 1, 'RSA con n=3337 y e=79', '1019', 22),
(46, 1, 'RSA con n=3337 y e=79', '1019', 22),
(47, 1, 'RSA con n=3337 y e=79', '1019', 22),
(48, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:3242532425,Cores:2', '[[5 2] [11 1] [1297 1] [9091 1]]\n', 22),
(49, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:32524236324326324,Cores:2', '[[2 2] [673 1] [12081811413197 1]]\n', 22),
(50, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:342523452,Cores:2', '[[2 2] [3 1] [23 1] [1241027 1]]\n', 22),
(51, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:32524236324326324,Cores:2', '[[2 2] [673 1] [12081811413197 1]]\n', 22),
(52, 0, 'RSA con n=3337 y e=79', '', 22),
(53, 0, 'RSA con n=3337 y e=79', '', 22),
(54, 0, 'RSA con n=3337 y e=79', '', 22),
(55, 0, 'RSA con n=3337 y e=79', '', 22),
(56, 0, 'RSA con n=3337 y e=79', '', 22),
(57, 0, 'RSA con n=3337 y e=79', '', 22),
(58, 0, 'RSA con n=3337 y e=79', '', 22),
(59, 0, 'RSA con n=3337 y e=79', '', 22),
(60, 0, 'RSA con n=3337 y e=79', '', 22),
(61, 1, 'RSA con n=3337 y e=79', '1019', 22),
(62, 1, 'RSA con n=3337 y e=79', '1019', 22),
(63, 1, 'RSA con n=3337 y e=79', '1019', 22),
(64, 1, 'RSA con n=3337 y e=79', '1019', 22),
(65, 1, 'RSA con n=3337 y e=79', '1019', 22),
(66, 1, 'RSA con n=3337 y e=79', '1019', 22),
(67, 1, 'RSA con n=3337 y e=79', '1019', 22),
(68, 1, 'RSA con n=3337 y e=79', '1019', 22),
(69, 1, 'RSA con n=3337 y e=79', '1019', 22),
(70, 1, 'RSA con n=3337 y e=79', '1019', 22),
(71, 1, 'RSA con n=3337 y e=79', '1019', 22),
(72, 1, 'RSA con n=3337 y e=79', '1019', 22),
(73, 1, 'RSA con n=3337 y e=79', '1019', 22),
(74, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:2346523,Cores:2', '[]\n', 22),
(75, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:2346522,Cores:2', '[[2 1] [3 1] [47 1] [53 1] [157 1]]\n', 22),
(76, 1, 'RSA con n=3337 y e=79', '1019', 22),
(77, 1, 'RSA con n=3337 y e=79', '1019', 22),
(78, 1, 'RSA con n=3337 y e=79', '1019', 22),
(79, 1, 'RSA con n=3337 y e=79', '1019', 22),
(80, 0, 'RSA con n=3337 y e=79', '', 22),
(81, 0, 'RSA con n=3337 y e=79', '', 22),
(82, 0, 'RSA con n=3337 y e=79', '', 22),
(83, 2, 'RSA con n=3337 y e=79', 'Tarea usó mayor tiempo del permitido!', 22),
(84, 2, 'RSA con n=3337 y e=79', 'Tiempo permitido excedido', 22),
(85, 2, 'RSA con n=3337 y e=79', 'Tiempo permitido excedido', 22),
(86, 2, 'RSA con n=3337 y e=79', 'Tiempo permitido excedido', 22),
(87, 2, 'RSA con n=3337 y e=79', 'Tiempo permitido excedido', 22),
(88, 2, 'RSA con n=3337 y e=79', 'Tiempo permitido excedido', 22),
(89, 1, 'RSA con n=3337 y e=79', '[[2 2] [673 1] [12081811413197 1]]\n', 22),
(90, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:3243242342324,Cores:2', '[[2 2] [41 1] [193 1] [523 1] [195919 1]]\n', 22),
(91, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:15,Cores:3', '[[3 1] [5 1]]\n', 24),
(92, 1, 'Algoritmo: Logaritmo Discreto,Introduzca número a calcular el logaritmo:13,Grupo p (primo):17,Generador:2,Cores:3', '6\n', 24),
(93, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:131075,Cores:5', '[[5 2] [7 2] [107 1]]\n', 24),
(94, 1, 'Algoritmo: Colisiones de hash,Hash a usar:MD5,Bytes truncacion (0 si no se trunca):2', '00010110 00010100 01011001 00011111 \n00111000 01011100 11111111 00111100 \n', 22),
(95, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:342432,Cores:2', '2^5·3^2·29^1·41^1', 22),
(96, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:324234,Cores:2', '2·3^2·18013', 22),
(97, 2, 'RSA con n=3337 y e=79', 'Tiempo permitido excedido', 22),
(98, 1, 'RSA con n=3337 y e=79', '1019', 22),
(99, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:8976767676,Cores:2', '2^2·3·1453·514841', 22),
(100, 1, 'Algoritmo: Colisiones de hash,Hash a usar:SHA3-224,Bytes truncacion (0 si no se trunca):4,Bytes en cero:2,Cores:2', '01000010 00101101 11110000 01000101 \n00111001 01011100 11110100 00111010 \n0x422df045\n0x395cf43a\n', 22),
(101, 1, 'RSA con n=3337 y e=79', '1019', 22),
(102, 2, 'RSA con n=3337 y e=79', 'Tiempo permitido excedido', 22),
(103, 1, 'Algoritmo: Colisiones de hash,Hash a usar:SHA1,Bytes truncacion (0 si no se trunca):4,Bytes en cero:2,Cores:2', '0x73edf8de\n0x76bfcc6c\n', 22),
(104, 1, 'Algoritmo: Preimagenes hash,Hash a usar:MD5,Secuencia a encontrar una preimagen:FFFF,Bytes truncacion (0 si no se trunca):2,Cores:2', '0x76645094338123253b01d5f80755e72b98bd08d55cf780928ceccb213814329facc80cb449dbae12f000ea5d6ce16f029a7911f89a4211383d6b7ed8cfc79d6639e35960aeda8e2f50e97f1b774bd9201f92f78bedf7ae21934e36ed03af0317c487dbf913199292\n', 22),
(105, 1, 'Algoritmo: Preimagenes hash,Hash a usar:SHA1,Secuencia a encontrar una preimagen:fffff,Bytes truncacion (0 si no se trunca):2,Cores:2', '0xa48a94d4327d32d513045ae8e07758b4749c152c006572f71b3895eb93cd8206d0e522d8b99b24faa8ce7c2aa57f422ab9425cfac90a97806c7bde454ab0b1b7773b782b176eec0a9e556d377777e741448234e78edde5090c0c7b616ba9300072455f1df27df484cb58a0551c696fbb9cc637cbb9d089a50df76ffeb11644f824bb8e209230457330ab199d61d1e3a6280effc385b7a4887bfed8b2d85d964161eca18594b8f77f50efb30613067b64fa84edd6cbb01a3282c6d216a2dcce2fcc24cf6339b110298c0048166f1d47f735e7182e444a954b8db27ba56bf56429e7550cc54f5da98576cea6dce53eb2765de572fa2c48e239dd49615c9c2456103fdd412e21fb2182886a30b7c39d624ad7d1ad3fcd538d2c5ece393eb382a1f7e24dbd610f7b13ea6dbfadbe7804ac3b65e5afab839e9d8961a83fe9c5c8d20b1a5267fe\n', 22),
(106, 1, 'Algoritmo: Factorización,Introduzca número a factorizar:324324,Cores:2', '2^2·3^4·7·11·13', 22),
(107, 1, 'RSA con n=3337 y e=79', '1019', 22),
(108, 1, 'RSA con n=3337 y e=79', '1019', 22),
(109, 2, 'RSA con n=3337 y e=79', 'Tiempo permitido excedido', 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_usuario` varchar(255) NOT NULL,
  `semilla` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL,
  `razon_ingreso` text NOT NULL,
  `tiene_acceso` tinyint(1) NOT NULL DEFAULT '0',
  `es_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre_usuario`, `semilla`, `password`, `razon_ingreso`, `tiene_acceso`, `es_admin`) VALUES
(1, 'hola', '28dc20f5ceb1c168b3da6aaa407621af18e3e3d711ccea3823113135dcd10838', 'wSZYOE6LngLcJWv7ByS84+FB4spOuhEQzOe5koiedGYFKO7C3SGHFecH+EwcwvMbDoLPIB/A3ukAHPrjIpkusA==', '', 0, 0),
(2, 'hola2', '28df887695add56254f591113bdeba03b60231ae5ec29aef3bd7e6b7d8f32aa6', 'v0QiBXmqHWnKVXOON6KIgcvshwHdT+U5TmMjv9l1AQrVEbtHOK72rAxCDWRN9CC3lXOrGoYHHV+0mzx2WMs1KQ==', '', 0, 0),
(3, 'hola3', 'cc9a8746859da92dc0d7cc523008e59bc7f79231bc36c83eee05b329bc5ca653', 'FHDkS8pHJjLwRUvjYmEVjQPLaYlhwPUt9jlQoElQYhZdbbU2vIlLcrzgvMeYYyS7yWH11cI/Awg9RSFWgk5blw==', '', 0, 0),
(4, 'hola3', '952d78e6625ff9d9c0b9d25e22f68b6a03fe1608e4b4dcf4202c21351cc722ac', 'Gsz9XsQ3YGuHCKb48+e1k8heeQjU6J7S89lvp26C1LGj+Y47ySV7hGD4naHvm9Z6+dSefblQ967qUY/53647rA==', '', 0, 0),
(5, 'hola3', 'aa83cf5b511ed0ecaa1a5c28e2beefff9edaace7b4b4c47c6f5ee38dbed859f7', 'q6NI40aFrcUf0RJiGwVlBkC59WOtHWGL4OPgaWT7bHWFtY5s/qWpUPibfovUc86fSQ42yA2k1WCoFA6FIj5oeA==', '', 0, 0),
(6, 'hola4', '4aa124b436378e7ef67c3fb1ea6a34e718a10d40ea24f612abd9b2c17acf5b3f', 'qQHoSV+uNDZ0ix346ROpv5H5aizP+jbJhFwrVtF5cQjIleVm/Y4cD9BEHDsWSyCHlAZKiTzi1Zkf59LBgrAmGw==', '', 0, 0),
(7, 'hola5', '125cf86050850ccfb98d2e82d13ef5bb49a0d32506e11b52e78451018b6ddb1d', 'CumbkNE2WW8nUITmOmMh+JsvNl6XK7YOA/QLWmXn9LCkPio/Q/I4eh/I6tMIkORf0hFDXyCoBRlTWyqgibvRNg==', '', 0, 0),
(8, 'asd', '3734f8889dc8a5d801d03d535343f8b914bd0ecbd6d435c6968f565529f14922', 'GAgOlw7nCwEOh84Rv6wZacxcLSasYSDcb9y77DXglYk9LoNOpiOF0iXG9CFPtIAfijW91YCVK3fwWnYCFt5JXw==', '', 0, 0),
(9, 'asdf', '4713ae39f76f98dfc246f62aebd2d2f01dcc9f062ff5a1071255fe49f2c2e69f', 'ETIYZuz0/7fi8qAv80tbP9lSpKGYHkA2MSHSbkUpQBNzE8ftxtX3elSX+9KC9q+DM6ARYafAhdRQsnx9WtjKbA==', '', 0, 0),
(10, 'qwerty', '7324b2c42f30120d7d832ec38d7eb8a89d8f5e91e35c4b7de221fc3286c26f85', 'JR0g0Jm065Rtz4F3EsaRMCpOpu4qdAu8x14Z8ecM1InB+eob4fWNZktgxzLDZ1BQwEOoM5xJjOEmV0xTg0IO6w==', '', 0, 0),
(11, 'qwe123', '94b559003f0c199e1676f263b139d1cdaeac8ee1b7a38c38db1a52f0bfc15774', 'KYCNeZjkt9ofiOW/3LHZSaaLtNvGj6uvWrAf90Uhc1GCq72i7BbONI9EFSpWY+DCpyhzlF0/B05svSwDKKZNTA==', '', 0, 0),
(12, 'asdsad', '80859ae4a377edccefaa7638cf4984d62e6baca2790f8ba43895df86d4666a2c', '6nKjr1oZ00C3ADRyO9nIedPBiBOUs+F2nNeUeDsTd/2dORLgv23hPbLxN9QsssYdOgg7PBTG3kPQQsEWu912wQ==', '', 0, 0),
(13, 'sdafgsadadg', 'ac2ed3711c2ee34d71ad92eab848e77d16724d691d152ec83eaadcef507374bb', 'uMiFTl+ovn23Btr9Xxg+uY9uGK56pbQdYwHlkPBLK1WoVfZzQakNHXTpqwwSFS82WuoZXOXd8jCJLNJN+GQKLw==', '', 0, 0),
(14, 'asdsadasdsa', '105bd9d0bf21646a008a30510e22bac66ea13bf49f44e6a1eebbc74293a25e0d', 'LEuZe+AwddlJ4ZtMxiBBNwusEF49Y+6mrJKSxEBXZ5vlIgO+uILbKIUcay3/9GfYaM/7f9I7FCMY7XVoG4sNXw==', '', 0, 0),
(15, 'sdadsafasdsa', '87743e6c2ecd57e996aec7cbd13919388b69e2634a31e83404c4c99fc58dd500', 'f00cWtMUJehSeVoYM9giW+tjUWT8YZuty7KA70yjt/sueKRwTdxhR30WBwcWK9dCOEDeVAvzTqHP6WzQJ8q63g==', '', 0, 0),
(16, 'fsdgsafsagasf', '9eaee91200728faafa80dd2bb413699e1d1da89c2de3e64d33da836cada44ccc', 'gxVDwOs8W8Y9ogtBFgvqaTma/kGtx8mX2riDrUM80qteEi9EeSFue1zwtTi9LzbZqI/3h3ACey4COaHOGosnPA==', '', 0, 0),
(17, 'dsafasgad', '2e5f1dcc23718e632a07387bba11f83e93b377f32264ab1834bed4621e2a8ef3', '6RO4vDsDqNzI4BdSXMUJA/vU3LhVeHN41orNXunfj6nrNL9BTSer5bQHQ/2w1M0e+l1RbZQsf+98UGIfN9mZdw==', '', 0, 0),
(18, 'asdasdashsfjdf', '1745770647e2f47e5cd25bdde01241f41aafd01d57bb351517a55233482070b9', 'yOjNzU6YmsGABl4QQYLXCq5cnMsLU1z8BwXv1EwqpI9L3N/I8z3B8ChOCsyB5Jha5d5vvx4G+2D0c+jtdT2KAQ==', '', 0, 0),
(19, 'SCALA1', '1', 'asd', '', 0, 0),
(21, 'SCALA10', '1', 'hola', '', 0, 0),
(22, 'SCALA11', '$2a$12$IpaGgLzqkQuFOBf/a92xN.', '$2a$12$IpaGgLzqkQuFOBf/a92xN.VZ7US71nnuxnf60UBwzPYVTrn7StZ.W', '', 1, 1),
(23, 'SCALA12', '$2a$12$N7x0kgx/w0zm.KASgYgNve', '$2a$12$N7x0kgx/w0zm.KASgYgNveMvOqNHHB7kjP1QZMo9d75.7i1Xc5UE6', '', 0, 0),
(24, 'ahevia', '$2a$12$cllD3xZ7K/bqT47kScysie', '$2a$12$cllD3xZ7K/bqT47kScysieXMDb2IMByXqHheVWBSoS3jGix.2SMWy', '', 0, 0),
(25, 'SCALA13', '$2a$12$6Q9z4sARRfuzI6KQl7rkje', '$2a$12$6Q9z4sARRfuzI6KQl7rkje71MnGLOJANMHYRHMCUbEVklyE26pdkS', '', 1, 0),
(26, 'SCALA14', '$2a$12$pBnYyx3PmC10Ee6TW/bWx.', '$2a$12$pBnYyx3PmC10Ee6TW/bWx.ihKDoNeiZSnjnInmVn6WkW77C3KcBzu', 'sadsad sdsad asdsad asdasd asdasd adadsa dasd', 1, 0);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `estados`
--
ALTER TABLE `estados`
  ADD CONSTRAINT `estados_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
