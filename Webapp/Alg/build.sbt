name := "Alg"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  "com.typesafe.play" %% "play-slick" % "0.5.0.8",
  "mysql" % "mysql-connector-java" % "5.1.26",
  "org.mindrot" % "jbcrypt" % "0.3m",
  "org.scala-lang" % "scala-compiler" % "2.10.2",
  "joda-time" % "joda-time" % "2.3",
  "com.github.tototoshi" %% "slick-joda-mapper" % "0.4.1"
)     

play.Project.playScalaSettings
