# --- Created by Slick DDL
# To stop Slick DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table `usuarios` (`id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,`nombre_usuario` VARCHAR(254) NOT NULL,`semilla` VARCHAR(254) NOT NULL,`password` VARCHAR(254) NOT NULL);

# --- !Downs

drop table `usuarios`;

