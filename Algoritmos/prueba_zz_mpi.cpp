#include <NTL/ZZ.h>
#include <mpi.h>


using namespace NTL;
using namespace std;

int main(){
	int rank;
	int size;
	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);


	ZZ *n;
	if(rank == 0){
		n = new ZZ();
		*n = to_ZZ(2);

		MPI_Send(n->rep, n->MaxAlloc()*NTL_ZZ_NBITS/8, MPI_BYTE, 1, 0, MPI_COMM_WORLD);

	}
	else{
		MPI_Status status;
		int amount;

		MPI_Probe(0, 0, MPI_COMM_WORLD, &status);

		MPI_Get_count(&status, MPI_BYTE, &amount);
		n = new ZZ();
		n->rep = (NTL_verylong)malloc(amount);
		MPI_Recv(n->rep, amount, MPI_BYTE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		cout << *n << endl;
	}

	MPI_Finalize();
}