#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1

#include "hash_functions.h"
#include <mpi.h>
#include <sstream>
#include <iostream>
#include <bitset>
#include <tr1/unordered_map>
#include <iomanip>

using namespace std;
//using namespace CryptoPP;
using namespace std::tr1;

typedef CryptoPP::AutoSeededRandomPool AutoSeededRandomPool;
typedef CryptoPP::word32 word32;

void pbits(std::string const& s) { 
    for(std::size_t i=0; i<s.size(); i++) 
        std::cout << std::bitset<CHAR_BIT>(s[i]) << " "; 
    cout << endl;
} 

void phex(std::string const& s) { 
	cout << "0x";
    for(std::size_t i=0; i<s.size(); i++) 
        cout << std::setfill('0') << std::setw(2) << std::setprecision(2) 
				<< std::hex << static_cast<unsigned int>(s[i] & 0xff); 
    cout << endl;
}

std::string toStr(const byte *s, int length) {
	int s_len = strlen((const char *)s);

	string in((const char *)s, s_len);
	while(s_len/2 < length){
		in = '0' + in;
		s_len++;
	}
    
    std::string output;

    size_t cnt = in.length() / 2;

    for (size_t i = 0; cnt > i; ++i) {
        unsigned int v = 0;
        std::stringstream ss;
        ss << std::hex << in.substr(i * 2, 2);
        ss >> v;

        output.push_back(static_cast<unsigned char>(v));
    }

    return output;
}

bool VerifyEquality(const byte *a, const byte *b, int length){
	for(int i = 0; i < length; i++){
		if(a[i] != b[i])
			return false;
	}
	return true;
}

string myres;
int set_my_res = 0;

template<class T>
void findPreimage(const byte *hash, int bytes_digest, int rank, int size){
	int flag;

	AutoSeededRandomPool rng;
	unsigned int bytes_input;

	byte fx[bytes_digest];


	while(true){
		bytes_input = rng.GenerateWord32(1, 512);
		byte x[bytes_input];
		rng.GenerateBlock(x, bytes_input);

		T::TruncatedHash(fx, bytes_digest, x, bytes_input);

		if(VerifyEquality(fx, hash, bytes_digest)){
			//lo mando
			int j = 1;
			for(int i = 0; i < size; i++){
				if(i != rank)
					MPI_Send(&j, 1, MPI_INT, i, i, MPI_COMM_WORLD);
			}
			if(rank == size-1){
				myres = string((const char *)x, bytes_input);
				set_my_res = 1;
			}
			else
				MPI_Send(x, bytes_input, MPI_UNSIGNED_CHAR, size-1, size, MPI_COMM_WORLD);
			break;
		}

		MPI_Iprobe(MPI_ANY_SOURCE, rank, MPI_COMM_WORLD, &flag, MPI_STATUS_IGNORE);
		if(flag){
			return;
		}
	}

}


int main(int argc, char **argv){
	int rank;
	int size;

	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);


	if(argc != 4){
		if(rank == size - 1){
			cout << "Debe introducir el algoritmo de hash a usar, el valor en hexadecimal a encontrar la preimagen, ";
			cout << "y la cantidad de bytes a lo cual se quiere truncar el hash (0 si no)" << endl;
		}
		return MPI_Finalize();
	}

	string h = argv[1];

	byte *hex_hash = (byte *)argv[2];

	int tr = atoi(argv[3]);

	int size_hash;

	void (*g)(const byte *, int, int, int);

	if(h == "SHA1"){
		size_hash = SHA1::DIGESTSIZE;
		
		g = findPreimage<SHA1>;
	}else if(h == "MD5"){
		size_hash = MD5::DIGESTSIZE;
		
		g = findPreimage<MD5>;
	}else if(h == "SHA224"){
		size_hash = SHA224::DIGESTSIZE;
		
		g = findPreimage<SHA224>;
	}else if(h == "SHA256"){
		size_hash = SHA256::DIGESTSIZE;
		
		g = findPreimage<SHA256>;
	}else if(h == "SHA384"){
		size_hash = SHA384::DIGESTSIZE;
		
		g = findPreimage<SHA384>;
	}else if(h == "SHA512"){
		size_hash = SHA512::DIGESTSIZE;
		
		g = findPreimage<SHA512>;
	}else if(h == "MD2"){
		size_hash = MD2::DIGESTSIZE;
		
		g = findPreimage<MD2>;
	}else if(h == "MD4"){
		size_hash = MD4::DIGESTSIZE;
		
		g = findPreimage<MD4>;
	}else if(h == "Tiger"){
		size_hash = Tiger::DIGESTSIZE;
		
		g = findPreimage<Tiger>;
	}else if(h == "Whirlpool"){
		size_hash = Whirlpool::DIGESTSIZE;
		
		g = findPreimage<Whirlpool>;
	}else if(h == "RIPEMD160"){
		size_hash = RIPEMD160::DIGESTSIZE;
		
		g = findPreimage<RIPEMD160>;
	}else if(h == "RIPEMD320"){
		size_hash = RIPEMD320::DIGESTSIZE;
		
		g = findPreimage<RIPEMD320>;
	}else if(h == "RIPEMD128"){
		size_hash = RIPEMD128::DIGESTSIZE;
		
		g = findPreimage<RIPEMD128>;
	}else if(h == "SHA3-224"){
		size_hash = SHA3_224::DIGESTSIZE;
		
		g = findPreimage<SHA3_224>;
	}else if(h == "SHA3-256"){
		size_hash = SHA3_256::DIGESTSIZE;
		
		g = findPreimage<SHA3_256>;
	}else if(h == "SHA3-384"){
		size_hash = SHA3_384::DIGESTSIZE;
		
		g = findPreimage<SHA3_384>;
	}else if(h == "SHA3-512"){
		size_hash = SHA3_512::DIGESTSIZE;
		
		g = findPreimage<SHA3_512>;
	}else{
		if(rank == size - 1){
			cout << "ERROR: Introduzca un algoritmo valido" << endl;
		}
		return MPI_Finalize();
	}

	int bytes_digest = tr == 0 ? size_hash : tr;
	string hash = toStr(hex_hash, bytes_digest);

	if(rank == size - 1){
		g((const byte *)hash.c_str(), bytes_digest, rank, size);
		if(set_my_res){
			phex(myres);
		}
		else{	
			int bytes_preimage;

			MPI_Status status;

			MPI_Probe(MPI_ANY_SOURCE, size, MPI_COMM_WORLD, &status);
			MPI_Get_count(&status, MPI_UNSIGNED_CHAR, &bytes_preimage);

			byte preimage[bytes_preimage];
			MPI_Recv(preimage, bytes_preimage, MPI_UNSIGNED_CHAR, MPI_ANY_SOURCE, size, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			string s((const char*)preimage, bytes_preimage);

			phex(s);
		}		
	}
	else{
		g((const byte *)hash.c_str(), bytes_digest, rank, size);
	}
	
	

	/*string a((char *)fx, SHA::DIGESTSIZE);
	cout << a.size() << endl;
	pbits(a, SHA::DIGESTSIZE);

	byte fy[SHA::DIGESTSIZE];

	SHA().CalculateDigest(fy, fx, SHA::DIGESTSIZE);

	string b((char *)fy, SHA::DIGESTSIZE);
	cout << b.size() << endl;
	pbits(b, SHA::DIGESTSIZE);

	SHA().CalculateDigest(fx, fx, SHA::DIGESTSIZE);

	string c((char *)fx, SHA::DIGESTSIZE);
	cout << c.size() << endl;
	pbits(c, SHA::DIGESTSIZE);

	if(VerifyEquality(fx, fy, SHA::DIGESTSIZE))
		cout << "error" << endl;*/


	//pbits(s);

	MPI_Finalize();
}
