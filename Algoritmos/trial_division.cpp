#include <NTL/ZZ.h>
#include <NTL/vector.h>
#include <NTL/pair.h>
#include <mpi.h>
#include <unistd.h>
#include <cstring>

using namespace std;
using namespace NTL;

int bytesVector(Vec<Pair<ZZ, ZZ> > &v){
	int sum = 0;
	for(int i = 0; i < v.length(); i++){
		sum += (v[i].a.MaxAlloc() + v[i].b.MaxAlloc())*NTL_ZZ_NBITS/8;
	}

	return sum;
}

void MPI_Send_vec(Vec<Pair<ZZ, ZZ> > &v, int size, int rank){
	int bytes = bytesVector(v);
	if(v.length() == 0){
		MPI_Send(NULL, 0, MPI_INT, size, rank*2, MPI_COMM_WORLD);
		MPI_Send(NULL, 0, MPI_BYTE, size, rank*2 + 1, MPI_COMM_WORLD);

		return;
	}

	NTL_verylong vl = (NTL_verylong)malloc(bytes);
	int tamanos[2*v.length()];

	int sum = 0;
	for(int i = 0; i < 2*v.length(); i+=2){
		tamanos[i] = v[i/2].a.MaxAlloc()*NTL_ZZ_NBITS/8;
		tamanos[i+1] = v[i/2].b.MaxAlloc()*NTL_ZZ_NBITS/8;

		memcpy(((char *)vl) + sum, v[i/2].a.rep, tamanos[i]);
		sum += tamanos[i];
		memcpy(((char *)vl) + sum, v[i/2].b.rep, tamanos[i+1]);
		sum += tamanos[i+1];
	}

	MPI_Send(tamanos, 2*v.length(), MPI_INT, size, rank*2, MPI_COMM_WORLD);
	MPI_Send(vl, bytes, MPI_BYTE, size, rank*2 + 1, MPI_COMM_WORLD);
}

Vec <Pair<ZZ, ZZ> > myfactors;

void factor(ZZ n, int rank, int size){
	
 
	Vec<Pair<ZZ, ZZ> > factors;
	factors.SetLength(10);

	ZZ p;
	p = rank == 0? to_ZZ(2) : (SqrRoot(n)*rank/size + 1);
	if(p%2 == 0 && p != 2)
		p = p + 1;


	int i = 0;
	ZZ a;
	ZZ m;
	int l = factors.length();

	while(p*p <= n*(rank+1)*(rank+1)/(size*size) && p*p > n*rank*rank/(size*size)){
		if(ProbPrime(p) && n%p == 0){
			a = 0;
			m = n;
			do{
				m = m/p;
				a++;
			}while(m%p == 0);

			if(l == i){
				l = 2*l;
				factors.SetLength(l);
			}

			if(rank+1 == size)
				n = m;

			factors[i].a = p;
			factors[i].b = a;
			i++;
		}
		if(p == 2)
			p = 3;
		else
			p = p+2;
	}

	factors.SetLength(i);
	
	if(rank != size - 1)
		MPI_Send_vec(factors, size-1, rank);
	else
		myfactors = factors;
} 


int main(int argc, char** argv){
	int rank;
	int size;


	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if(argc != 2){
		if(rank == size - 1)
			cout << "Debe introducir un número a factorizar" << endl;
		return MPI_Finalize();
	}

	ZZ n = to_ZZ(argv[1]);

	Vec<Pair<ZZ, ZZ> > vs[size];
	
	
	if(rank == size - 1){
		factor(n, rank, size);
		for(int i = 0; i < size-1; i++){
			MPI_Status status;
			int amount;

		    /*FACTORES*/
		    int tamano_tamanos;

		    MPI_Probe(i, i*2, MPI_COMM_WORLD, &status);

		    MPI_Get_count(&status, MPI_INT, &tamano_tamanos);

		    if(tamano_tamanos == 0)
		    	tamano_tamanos = 1;

		    int tamanos[tamano_tamanos];

		    MPI_Recv(tamanos, tamano_tamanos, MPI_INT, i, i*2, MPI_COMM_WORLD,
		             MPI_STATUS_IGNORE);


		    MPI_Probe(i, i*2 + 1, MPI_COMM_WORLD, &status);

		    MPI_Get_count(&status, MPI_BYTE, &amount);

		    NTL_verylong vl = NULL;

		    if(amount != 0)
		    	 vl = (NTL_verylong)malloc(amount);

		    MPI_Recv(vl, amount, MPI_BYTE, i, i*2+1, MPI_COMM_WORLD,
		             MPI_STATUS_IGNORE);
		    
		    int l = tamano_tamanos/2;

		    vs[i].SetLength(l);
			int sum = 0;
		    for(int k = 0; k < l; k++){
		    	vs[i][k].a.rep = (NTL_verylong)malloc(tamanos[2*k]);
		    	memcpy(vs[i][k].a.rep, ((char *)vl) + sum, tamanos[2*k]);
		    	sum += tamanos[2*k];

		    	vs[i][k].b.rep = (NTL_verylong)malloc(tamanos[2*k+1]);
		    	memcpy(vs[i][k].b.rep, ((char *)vl) + sum, tamanos[2*k+1]);
		    	sum += tamanos[2*k+1];
		    }


		    if(l != 0){
		    	free(vl);
		    }

		}
		vs[size-1] = myfactors;

		Vec<Pair<ZZ, ZZ> > vk = vs[0];
		for(int i = 1; i < size; i++){
			append(vk, vs[i]);
		}
		ZZ pk = to_ZZ(1);
		Pair<ZZ, ZZ> p_pk;

		for(int i = 0; i < vk.length(); i++){
			for(int j = 0; j < vk[i].b; j++)
				pk *= vk[i].a;
		}

		p_pk.a = n/pk;
		p_pk.b = 1;

		if(pk != 1 && pk != n)
			append(vk, p_pk);

		if(vk.length() == 0){
			p_pk.a = n;
			p_pk.b = 1;
			append(vk, p_pk);
		}

		cout << vk << endl;
		
	}
	else{
		factor(n, rank, size);
	}
	
	MPI_Finalize();
}
