#include <NTL/ZZ_p.h>
#include <NTL/ZZ.h>
#include <mpi.h>

using namespace std;
using namespace NTL;

ZZ myres = to_ZZ(-1);
int flag;

void DL(ZZ_p x, ZZ_p base, int rank, int size){
	ZZ mod = ZZ_p::modulus();
	ZZ_p r;
	r = power(base, (mod-1)*rank/size);

	MPI_Request req_recv;

	for(ZZ y = (mod-1)*rank/size; y < (mod-1)*(rank+1)/size; y++){
		if(r == x){
			int j = 1;
			for(int i = 0; i < size; i++){
				if(i != rank)
					MPI_Send(&j, 1, MPI_INT, i, i, MPI_COMM_WORLD);
			}
			flag = 1;
			if(rank == size-1)
				myres = y;
			else
				MPI_Send(y.rep, y.MaxAlloc()*NTL_ZZ_NBITS/8, MPI_BYTE, size-1, size, MPI_COMM_WORLD);
			return;
			
		}
		MPI_Iprobe(MPI_ANY_SOURCE, rank, MPI_COMM_WORLD, &flag, MPI_STATUS_IGNORE);
		if(flag){
			return;
		}
		r = base*r; 
	}
}

int main(int argc, char** argv){
	int rank;
	int size;

	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if(argc != 4){
		if(rank == size - 1)
			cout << "Debe introducir el número a calcular el logaritmo, el grupo, y un generador" << endl;
		
		return MPI_Finalize();
	}

	ZZ res;
	ZZ p = to_ZZ(argv[2]);
	ZZ_p::init(p);

	if(rank == size-1){
		DL(to_ZZ_p(to_ZZ(argv[1])), to_ZZ_p(to_ZZ(argv[3])), rank, size);
		
		if(myres != to_ZZ(-1)){
			res = myres;
		}
		else{
			int amount;
			MPI_Status status;

			MPI_Probe(MPI_ANY_SOURCE, size, MPI_COMM_WORLD, &status);
			MPI_Get_count(&status, MPI_BYTE, &amount);

			res.rep = (NTL_verylong)malloc(amount);

			MPI_Recv(res.rep, amount, MPI_BYTE, MPI_ANY_SOURCE, size, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			
		}
		cout << "res: " << res << endl;
	}
	else{
		DL(to_ZZ_p(to_ZZ(argv[1])), to_ZZ_p(to_ZZ(argv[3])), rank, size);
		if(!flag)
			MPI_Probe(MPI_ANY_SOURCE, rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}


	MPI_Finalize();
}
