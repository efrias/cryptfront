#include <ecn.h>
#include <big.h>
#include <NTL/ZZ.h>
#include <sstream>
#include <mpi.h>
#include "schoof.h"

using namespace std;
using namespace NTL;

//Miracl precision(100, 10);
ZZ myres;
int flag;

void DL(ECn y, ECn g, int rank, int size, Big max_elems){
	int zz_s_length;

	ECn _y;
	Big k = max_elems*rank/size;

	stringstream ss;

	ss << max_elems;

	ZZ max_elems_zz = to_ZZ(ss.str().c_str());
	ZZ p_zz = to_ZZ(ss.str().c_str()); 

	_y = k*g; 

	MPI_Request req_recv;

	for(ZZ x = max_elems_zz*rank/size; x < max_elems_zz*(rank+1)/size && (x == 0 || !_y.iszero()); x++){
		if(_y == y){
			int j = 1;
			for(int i = 0; i < size; i++){
				if(i != rank)
					MPI_Send(&j, 1, MPI_INT, i, i, MPI_COMM_WORLD);
			}
			
			flag = 1;
			if(rank == size-1)
				myres = x;
			else
				MPI_Send(x.rep, x.MaxAlloc()*NTL_ZZ_NBITS/8, MPI_BYTE, size-1, size, MPI_COMM_WORLD);
			return;
		}

		MPI_Iprobe(MPI_ANY_SOURCE, rank, MPI_COMM_WORLD, &flag, MPI_STATUS_IGNORE);
		if(flag){
			return;
		}

		_y += g;
	}

}

int main(int argc, char **argv){
	int rank;
	int size;
	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if(argc != 8){
		if(rank == size - 1){
			cout << "Debe introducir el punto x y al cual calcular su logaritmo, parametros A, B de la curva, ";
			cout << "un primo del anillo sobre el cual se define la curva y un generador, w z" << endl;
		}
		
		return MPI_Finalize();
	}

	ZZ res;

	Big a(argv[3]);
	Big b(argv[4]);
	Big p(argv[5]);

	if(rank == size-1){
		Big max_elems = schoof(p, a, b);

		stringstream ss;
		ss << max_elems;

		int length_me = ss.str().length() + 1;
		char *max_elems_s = new char[length_me];
		strcpy(max_elems_s, ss.str().c_str());
		

		for(int i = 0; i < size-1; i++){
			MPI_Send(max_elems_s, length_me, MPI_CHAR, i, size + i + 1, MPI_COMM_WORLD);
		}

		free(max_elems_s);

		ecurve(a, b, p, MR_PROJECTIVE);

                Big x(argv[1]);
                Big y(argv[2]);

                Big w(argv[6]);
                Big z(argv[7]);

                ECn q(x, y);
                ECn g(w, z);


                DL(q, g, rank, size, max_elems);

		if(myres != to_ZZ(-1)){
			res = myres;
		}
		else{
			int amount;
			MPI_Status status;

			MPI_Probe(MPI_ANY_SOURCE, size, MPI_COMM_WORLD, &status);
			MPI_Get_count(&status, MPI_BYTE, &amount);

			res.rep = (NTL_verylong)malloc(amount);

			MPI_Recv(res.rep, amount, MPI_BYTE, MPI_ANY_SOURCE, size, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		cout << res << endl;

		
	}
	else{
		
		int amount;
		MPI_Status status;

		MPI_Probe(size-1, size + rank, MPI_COMM_WORLD, &status);
		MPI_Get_count(&status, MPI_CHAR, &amount);

		char *max_elems_s = (char *)malloc(amount);

		MPI_Recv(max_elems_s, amount, MPI_CHAR, size-1, size + rank + 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		Big max_elems = Big(max_elems_s);

		ecurve(a, b, p, MR_PROJECTIVE);

		Big x(argv[1]);
		Big y(argv[2]);

		Big w(argv[6]);
		Big z(argv[7]);

		ECn q(x, y);
		ECn g(w, z);

		

		DL(q, g, rank, size, max_elems);

		if(!flag)
			MPI_Probe(MPI_ANY_SOURCE, rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}


	MPI_Finalize();

}
