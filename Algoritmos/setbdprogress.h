// Debemos incluir las siguientes cabeceras
#include <mysql_connection.h>
#include <cppconn/driver.h>
#include <cppconn/resultset.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/exception.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

using namespace sql;
using namespace std;

//used when it is not important that multiple process overwrite the progress value
void setProgress(const char *s){
	Driver *driver;
    Connection *connection;
    PreparedStatement *ps;
    ResultSet *resultset;

    pid_t pid = getpid();

    driver = get_driver_instance();
    connection = driver->connect("tcp://127.0.0.1:3306", "root", "emeraldsword68");

    connection->setSchema("cryptfront_bd");

    ps = connection->prepareStatement("UPDATE estados SET progreso=? WHERE pid=? AND listo=0");
    ps->setString(1, s);
    ps->setInt(2, pid);
    ps->executeUpdate();

    //delete resultset;
    delete ps;
    delete connection;
}

//used when you need to operate in some way with the actual progress value in the db
template<class Op>
void updateProgress(Op f){
	Driver *driver;
    Connection *connection;
    PreparedStatement *ps;
    ResultSet *resultset;

    string s;
    pid_t pid = getpid();

    driver = get_driver_instance();
    connection = driver->connect("tcp://127.0.0.1:3306", "root", "emeraldsword68");

    connection->setSchema("cryptfront_bd");

    ps = connection->prepareStatement("SELECT progreso FROM estados WHERE pid=? AND listo=0");
    ps->setInt(1, pid);
    resultset = ps->executeQuery();

    while(resultset->next()){
    	s = f(resultset->getString(1));
    }

    delete ps;

    ps = connection->prepareStatement("UPDATE estados SET progreso=? WHERE pid=? AND listo=0");
    ps->setString(1, s.c_str());
    ps->setInt(2, pid);
    ps->executeUpdate();

    delete resultset;
    delete ps;
    delete connection;
    return s;
}