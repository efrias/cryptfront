#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1

#include <cryptopp/sha.h>
#include <cryptopp/sha3.h>
#include <cryptopp/md2.h>
#include <cryptopp/md4.h>
#include <cryptopp/md5.h>
#include <cryptopp/tiger.h>
#include <cryptopp/whrlpool.h>
#include <cryptopp/ripemd.h>
#include <cryptopp/osrng.h>


/* Clase Hash no es usada explicitamente, sino que sirve como "interfaz" al crear otra clase (o wrapper) que 
	calcule un "digest", la cual debe tener un método que indique la cantidad de bytes del hash, y otro que calcule
	el hash truncado de una secuencia de bytes*/


// typedef unsigned char byte
class Hash{
	public:
		static const int DIGESTSIZE = 0;
		static void TruncatedHash(byte *y, int y_bytes, const byte *x, int x_bytes){}
		static const char *Name(){ return ""; }
};

template<class T>
class Hashes_CryptoPP : Hash{
	public:
		static const int DIGESTSIZE = T::DIGESTSIZE;
		static void TruncatedHash(byte *y, int y_bytes, const byte *x, int x_bytes){
			T().CalculateTruncatedDigest(y, y_bytes, x, x_bytes);
		}
		static const char *Name(){return T::StaticAlgorithmName();}
};

typedef Hashes_CryptoPP<CryptoPP::Weak1::MD2> MD2;
typedef Hashes_CryptoPP<CryptoPP::Weak1::MD4> MD4;
typedef Hashes_CryptoPP<CryptoPP::Weak1::MD5> MD5;
typedef Hashes_CryptoPP<CryptoPP::SHA> SHA1;
typedef Hashes_CryptoPP<CryptoPP::SHA224> SHA224;
typedef Hashes_CryptoPP<CryptoPP::SHA256> SHA256;
typedef Hashes_CryptoPP<CryptoPP::SHA384> SHA384;
typedef Hashes_CryptoPP<CryptoPP::SHA512> SHA512;
typedef Hashes_CryptoPP<CryptoPP::SHA3_224> SHA3_224;
typedef Hashes_CryptoPP<CryptoPP::SHA3_256> SHA3_256;
typedef Hashes_CryptoPP<CryptoPP::SHA3_384> SHA3_384;
typedef Hashes_CryptoPP<CryptoPP::SHA3_512> SHA3_512;
typedef Hashes_CryptoPP<CryptoPP::Tiger> Tiger;
typedef Hashes_CryptoPP<CryptoPP::Whirlpool> Whirlpool;
typedef Hashes_CryptoPP<CryptoPP::RIPEMD128> RIPEMD128;
typedef Hashes_CryptoPP<CryptoPP::RIPEMD160> RIPEMD160;
typedef Hashes_CryptoPP<CryptoPP::RIPEMD320> RIPEMD320;
