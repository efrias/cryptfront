#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1

#include "hash_functions.h"
#include <mpi.h>
#include <bitset>
#include <tr1/unordered_map>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <sstream>

using namespace std;
//using namespace CryptoPP;
using namespace std::tr1;

typedef CryptoPP::AutoSeededRandomPool AutoSeededRandomPool; 

void pbits(std::string const& s) { 
    for(std::size_t i=0; i<s.size(); i++) 
        std::cout << std::bitset<CHAR_BIT>(s[i]) << " "; 
    cout << endl;
} 

void phex(std::string const& s) { 
	cout << "0x";
    for(std::size_t i=0; i<s.size(); i++) 
        cout << std::setfill('0') << std::setw(2) << std::setprecision(2) 
				<< std::hex << static_cast<unsigned int>(s[i] & 0xff); 
    cout << endl;
} 

bool VerifyEquality(byte *a, byte *b, int length){
	for(int i = 0; i < length; i++){
		if(a[i] != b[i])
			return false;
	}
	return true;
}

template<class T>
pair<string, string> findCollisionInPaths(pair<string, long int> a, pair<string, long int> b, int bytes_digest){
	long long int al = a.second;
	long long int bl = b.second;

	byte ax[bytes_digest];
	byte bx[bytes_digest];	

	byte afx[bytes_digest];
	byte bfx[bytes_digest];

	memcpy(ax, a.first.c_str(), bytes_digest);
	memcpy(bx, b.first.c_str(), bytes_digest);

	long int dif = al - bl;

	int min = dif >= 0 ? al : bl;

	while(dif > 0){
		T::TruncatedHash(ax, bytes_digest, ax, bytes_digest);
		dif--;
	}
	while(dif < 0){
		T::TruncatedHash(bx, bytes_digest, bx, bytes_digest);
		dif++;
	}


	while(true){
		T::TruncatedHash(afx, bytes_digest, ax, bytes_digest);
		T::TruncatedHash(bfx, bytes_digest, bx, bytes_digest);


		if(VerifyEquality(afx, bfx, bytes_digest)){
			if(!VerifyEquality(ax, bx, bytes_digest))
				return pair<string, string>(string((char *)ax, bytes_digest), string((char *)bx, bytes_digest));
			else
				break;
		}

		memcpy(ax, afx, bytes_digest);
		memcpy(bx, bfx, bytes_digest);
	}

	return pair<string,string>("", "");
}

template<class T>
bool isDistinguished(byte *fx, int d, int bytes_digest){
	for(int i = 0; i < d; i++){
		if(fx[i] != 0)
			return false;
	}
	return true;
}

template<class T>
void findCollision(int rank, int size, int bytes_digest, int bytes_in_zero){
	int flag;

	AutoSeededRandomPool rng;
	byte x[bytes_digest];
	rng.GenerateBlock(x, bytes_digest);

	unordered_map<string, pair<string, long long int> > d_pts;

	byte fx[bytes_digest];

	memcpy(fx, x, bytes_digest);

	long long int path = 0;

	MPI_Request req[3];
	MPI_Status st[3];

	bool first = true;

	while(true){
		T::TruncatedHash(fx, bytes_digest, fx, bytes_digest);
		string s((char *)fx, bytes_digest);

		path++;

		if(isDistinguished<T>(fx, bytes_in_zero, bytes_digest)){
			/*pbits(s);
			cout << path << endl;*/
			if(!first)
				MPI_Waitall(3, req, st);

			MPI_Isend(fx, bytes_digest, MPI_UNSIGNED_CHAR, size-1, size - 1 + 3*rank, MPI_COMM_WORLD, &req[0]);
			MPI_Isend(x, bytes_digest, MPI_UNSIGNED_CHAR, size-1, size - 1 + 3*rank + 1, MPI_COMM_WORLD, &req[1]);
			MPI_Isend(&path, 1, MPI_LONG_LONG_INT, size-1, size - 1 + 3*rank+2, MPI_COMM_WORLD, &req[2]);
		
			/*pbits(s);

			cout << path << endl;*/

			path = 0;
			rng.GenerateBlock(x, bytes_digest);
			memcpy(fx, x, bytes_digest);

			first = false;

		}

		//ver si no debo seguir calculando
		MPI_Iprobe(size-1, rank, MPI_COMM_WORLD, &flag, MPI_STATUS_IGNORE);
		if(flag){
			MPI_Recv(&flag, 1, MPI_INT, size-1, rank, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			return;
		}

		if(path >= 20*65536){
			path = 0;
			rng.GenerateBlock(x, bytes_digest);
			memcpy(fx, x, bytes_digest);	
		}
	}
}

template<class T>
pair<string, string> distinguishedReceiver(int rank, int size, int bytes_digest, int bytes_in_zero){
	unordered_map<string, pair<string, long long int> > d_pts;


	byte digest[bytes_digest];
	byte x[bytes_digest];
	long long int path;

	while(true){
		MPI_Status status;

		MPI_Recv(digest, bytes_digest, MPI_UNSIGNED_CHAR, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		MPI_Recv(x, bytes_digest, MPI_UNSIGNED_CHAR, status.MPI_SOURCE, status.MPI_TAG + 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(&path, 1, MPI_LONG_LONG_INT, status.MPI_SOURCE, status.MPI_TAG + 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		string s((char *)digest, bytes_digest);

		if(d_pts.count(s) > 0){
			pair<string, long long int> sp(string((char *)x, bytes_digest), path); 
			pair<string, string>  ab = findCollisionInPaths<T>(sp, d_pts[s], bytes_digest);
			if(ab.first != string("") || ab.second != string("")){
					int j = 1;
					for(int i = 0; i < size-1; i++){
						MPI_Send(&j, 1, MPI_INT, i, i, MPI_COMM_WORLD);
					}

					//Guardar tamaño de los caminos, y cantidad de puntos distinguidos encontrarlos, para realizar
					//una verificacion de los tiempos teoricos. 

					#ifdef PATHS_DPS

					stringstream st1;
					stringstream st2;

					st1 << T::Name() << "_paths_bd=" << bytes_digest << "_dps=" << bytes_in_zero;
					st2 << T::Name() << "_dps_bd=" << bytes_digest << "_dps=" << bytes_in_zero; 

					ofstream paths(st1.str().c_str(), ios::app);
					ofstream dps(st2.str().c_str(), ios::app);

					dps << d_pts.size() << endl;
					dps.close();

					for(auto &kv : d_pts){
						paths << kv.second.second << endl;
					}

					paths.close();

					#endif

					return ab;
			}
		}
		else{
			d_pts[s] = pair<string, long long int>(string((char *)x, bytes_digest), path);
		}
	}
}

int main(int argc, char **argv){
	int rank;
	int size;

	pair<string, string>  ab;

	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);


	if(argc != 4){
		if(rank == size - 1){
			cout << "Debe introducir el algoritmo de hash a usar, a cuántos bytes truncarlo (0 si nada), ";
			cout << "y la cantidad de ceros de los puntos distinguidos" << endl;
		}
		return MPI_Finalize();
	}

	string h = argv[1];

	int tr = atoi(argv[2]);
	int bytes_in_zero = atoi(argv[3]);

	int size_hash;

	pair<string, string> (*f)(int, int, int, int);
	void (*g)(int, int, int, int);

	if(h == "SHA1"){
		size_hash = SHA1::DIGESTSIZE;
		f = distinguishedReceiver<SHA1>;
		g = findCollision<SHA1>;
	}else if(h == "MD5"){
		size_hash = MD5::DIGESTSIZE;
		f = distinguishedReceiver<MD5>;
		g = findCollision<MD5>;
	}else if(h == "SHA224"){
		size_hash = SHA224::DIGESTSIZE;
		f = distinguishedReceiver<SHA224>;
		g = findCollision<SHA224>;
	}else if(h == "SHA256"){
		size_hash = SHA256::DIGESTSIZE;
		f = distinguishedReceiver<SHA256>;
		g = findCollision<SHA256>;
	}else if(h == "SHA384"){
		size_hash = SHA384::DIGESTSIZE;
		f = distinguishedReceiver<SHA384>;
		g = findCollision<SHA384>;
	}else if(h == "SHA512"){
		size_hash = SHA512::DIGESTSIZE;
		f = distinguishedReceiver<SHA512>;
		g = findCollision<SHA512>;
	}else if(h == "MD2"){
		size_hash = MD2::DIGESTSIZE;
		f = distinguishedReceiver<MD2>;
		g = findCollision<MD2>;
	}else if(h == "MD4"){
		size_hash = MD4::DIGESTSIZE;
		f = distinguishedReceiver<MD4>;
		g = findCollision<MD4>;
	}else if(h == "Tiger"){
		size_hash = Tiger::DIGESTSIZE;
		f = distinguishedReceiver<Tiger>;
		g = findCollision<Tiger>;
	}else if(h == "Whirlpool"){
		size_hash = Whirlpool::DIGESTSIZE;
		f = distinguishedReceiver<Whirlpool>;
		g = findCollision<Whirlpool>;
	}else if(h == "RIPEMD160"){
		size_hash = RIPEMD160::DIGESTSIZE;
		f = distinguishedReceiver<RIPEMD160>;
		g = findCollision<RIPEMD160>;
	}else if(h == "RIPEMD320"){
		size_hash = RIPEMD320::DIGESTSIZE;
		f = distinguishedReceiver<RIPEMD320>;
		g = findCollision<RIPEMD320>;
	}else if(h == "RIPEMD128"){
		size_hash = RIPEMD128::DIGESTSIZE;
		f = distinguishedReceiver<RIPEMD128>;
		g = findCollision<RIPEMD128>;
	}else if(h == "SHA3-224"){
		size_hash = SHA3_224::DIGESTSIZE;
		f = distinguishedReceiver<SHA3_224>;
		g = findCollision<SHA3_224>;
	}else if(h == "SHA3-256"){
		size_hash = SHA3_256::DIGESTSIZE;
		f = distinguishedReceiver<SHA3_256>;
		g = findCollision<SHA3_256>;
	}else if(h == "SHA3-384"){
		size_hash = SHA3_384::DIGESTSIZE;
		f = distinguishedReceiver<SHA3_384>;
		g = findCollision<SHA3_384>;
	}else if(h == "SHA3-512"){
		size_hash = SHA3_512::DIGESTSIZE;
		f = distinguishedReceiver<SHA3_512>;
		g = findCollision<SHA3_512>;
	}else{
		if(rank == size - 1){
			cout << "ERROR: Introduzca un algoritmo valido" << endl;
		}
		return MPI_Finalize();
	}

	int bytes_digest = tr == 0 ? size_hash : tr;

	if(bytes_in_zero > bytes_digest){
		if(rank == size - 1){
			cout << "ERROR: La cantidad de bytes en cero (para los puntos distinguidos) no puede ser mayor que la cantidad";
			cout << " de bytes del hash!" << endl;
		}
		return MPI_Finalize();
	}
	#ifdef PATHS_DPS
	int total = 50;

	while(total >0){

	#endif
		if(rank == size - 1){
			ab = f(rank, size, bytes_digest, bytes_in_zero);
			if(ab.first != "" || ab.second != ""){
				//pbits(ab.first);
				//pbits(ab.second);
				phex(ab.first);
				phex(ab.second);
				
				//cout << hex << ab.second << endl;
			}		
		}
		else{
			g(rank, size, bytes_digest, bytes_in_zero);
		}
	#ifdef PATHS_DPS
		total--;
	}
	#endif 
	
	

	/*string a((char *)fx, SHA::DIGESTSIZE);
	cout << a.size() << endl;
	pbits(a, SHA::DIGESTSIZE);

	byte fy[SHA::DIGESTSIZE];

	SHA().CalculateDigest(fy, fx, SHA::DIGESTSIZE);

	string b((char *)fy, SHA::DIGESTSIZE);
	cout << b.size() << endl;
	pbits(b, SHA::DIGESTSIZE);

	SHA().CalculateDigest(fx, fx, SHA::DIGESTSIZE);

	string c((char *)fx, SHA::DIGESTSIZE);
	cout << c.size() << endl;
	pbits(c, SHA::DIGESTSIZE);

	if(VerifyEquality(fx, fy, SHA::DIGESTSIZE))
		cout << "error" << endl;*/


	//pbits(s);

	MPI_Finalize();
}