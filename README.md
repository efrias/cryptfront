About installation on AWS
===
There is a working version of Cryptfront using a cluster in the AWS EC2 cloud. There are three AMIs that are necessary for the system to work:

- ami-b29ff782: Web server AMI. It is used as an interface for the user and for queuing jobs to the AWS cluster.
- ami-489ff778: AMI of the main node of the cluster. It is used as the master node to run MPI jobs.
- ami-be9ff78e: AMI of other nodes of the cluster.

These AMIs contains the source code of the application, but it is probably not up-to-date. However, they provide a functional system.

About configuration on AWS
===
There are some details to take care of.

Firstly, there is a file called `hosts` in `/data` of the master node that contains a list of IPs of nodes of the cluster and the number of cores of each one. It is changed dynamically when node instances are started and stopped. However, the first line maps to the IP and number of cores of the master node, which is probably different to the current IP and number of cores.

In second place, the master node of the cluster hosts a NFS that contains source code and have some configuration files for the rests of the instances. So, in all of the instances but the master node, one must change `/etc/fstab` file, providing the correct private IP of the master node.
